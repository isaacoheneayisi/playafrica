<?php
// you might need to replace some paramenters in the xml, msisdn, amount, datetime
// billing xml

$now = new DateTime();
$cur =  $now->format(DateTime::ISO8601);


$xml = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/MFS/PurchaseInitiateRequest/V1" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3">
  <SOAP-ENV:Header xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    <cor:debugFlag xmlns:cor="http://soa.mic.co.af/coredata_1">true</cor:debugFlag>
    <wsse:Security>
      <wsse:UsernameToken>
        <wsse:Username>live_mw_mtech</wsse:Username>
        <wsse:Password>Mtech7356</wsse:Password>
      </wsse:UsernameToken>
    </wsse:Security>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <v1:PurchaseInitiateRequest>
      <v3:RequestHeader>
        <v3:GeneralConsumerInformation>
          <v3:consumerID>MTECH</v3:consumerID>
          <v3:transactionID>123456</v3:transactionID>
          <v3:country>GHA</v3:country>
          <v3:correlationID>2233445566</v3:correlationID>
        </v3:GeneralConsumerInformation>
      </v3:RequestHeader>
      <v1:requestBody>
        <v1:customerAccount>
          <v1:msisdn>233573946296</v1:msisdn>
        </v1:customerAccount>
        <v1:initiatorAccount>
          <v1:msisdn>233273404088</v1:msisdn>
        </v1:initiatorAccount>
        <v1:paymentReference>Test</v1:paymentReference>
        <v1:externalCategory>voice</v1:externalCategory>
        <v1:externalChannel>default</v1:externalChannel>
        <v1:webUser>war_approver</v1:webUser>
        <v1:webPassword>4088</v1:webPassword>
        <v1:merchantName>mtech</v1:merchantName>
        <v1:itemName>iPhone 6</v1:itemName>
        <v1:amount>0.2</v1:amount>
        <v1:minutesToExpire>30</v1:minutesToExpire>
        <v1:notificationChannel>2</v1:notificationChannel>
      </v1:requestBody>
    </v1:PurchaseInitiateRequest>
  </SOAP-ENV:Body>
  </SOAP-ENV:Envelope>';


# check balance xml
/*
$xml = '<?xml version="1.0" encoding="UTF-8"?>
<methodCall>
<methodName>GetBalanceAndDate</methodName>
<params>
<param>
<value>
<struct>
<member><name>originNodeType</name><value>AIR</value></member>
<member><name>subscriberNumber</name><value>233265002033</value></member>
<member><name>originTransactionID</name><value>52129019</value></member>
<member><name>originTimeStamp</name><value><dateTime.iso8601>20160413T03:40:26+0200</dateTime.iso8601></value></member>
<member><name>originHostName</name><value>fdsuser</value></member>
</struct>
</value>
</param>
</params>
</methodCall>
';
*/



// put the actual charging url
$url = "https://accessgw.tigo.com.gh:8443/live/PurchaseInitiate";
$xml_length = strlen($xml);

$headers = buildheaders($url, $xml_length);

$response = sendxml($url, $xml, $headers);
header("Content-type: text/xml");


echo $response;


function sendxml($url, $xml_post_string, $headers)
{
    
    
    $soapUser = "mtech3"; // replace with the actual user if any and if NOT comment line 55 out
    $soapPassword = "mtech3"; // replace with the actual password if any and if NOT comment line 55 out

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_USERPWD, $soapUser . ":" . $soapPassword);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_KEYPASSWD, "tigo123!");
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    // you can uncomment it and put the real path to .crt file if give.
    curl_setopt($ch, CURLOPT_CAINFO, 'cert.pem');

    // converting
    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}
function buildheaders($soapUrl, $xml_length)
{
    $headers = array(
        "POST /Air HTTP/1.1",
        "User-Agent:UGw Server/3.1/1.0",
        "Authorization: Basic b25tb2JpbGU6b25tb2JpbGU=",
        "Content-Type:text/xml",
        "Accept: text/xml",
        "Cache-Control: no-cache",
        "SOAPAction: " . $soapUrl,
        "Pragma: no-cache",
        "Content-length: " . $xml_length);

    return $headers;
}

?>

