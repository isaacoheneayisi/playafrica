/**
 * Created by Gideon Paitoo on 2/8/2016.
 */
$(document).ready(function(){

getartist();
    getalbums();
    getgenre();
});

var _URL = window.URL || window.webkitURL;

$("#simg").change(function(e) {
    var file, img;


    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            alert(this.width + " " + this.height);
            if(this.width != this.height){
                document.getElementById('simg').setCustomValidity('This Image is Not a Square');
            }else {
                document.getElementById('simg').setCustomValidity('');
            }

        };
        img.onerror = function() {
            alert( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);


    }

});

function getartist() {

    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'getartist.php',
        contentType: false,
        processData: false

    });
    ajaxRequest.done(function (xhr, textStatus) {
        var $select = $('#sartist');
        $select.empty();
        $.each(eval(xhr), function(i, val){
            $select.append($('<option />', { value: val.id, text: val.name }));
        });

    });
};

function getalbums() {

    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'getalbums.php',
        contentType: false,
        processData: false

    });
    ajaxRequest.done(function (xhr, textStatus) {
        var $select = $('#salbum');
        $select.empty();
        $.each(eval(xhr), function(i, val){
            $select.append($('<option />', { value: val.id, text: val.albumtitle }));
        });

    });
}

function getgenre() {

    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'getgenre.php',
        contentType: false,
        processData: false

    });
    ajaxRequest.done(function (xhr, textStatus) {

        var $select = $('#sgenre');
        $select.empty();
        $.each(eval(xhr), function(i, val){
            $select.append($('<option />', { value: val.id, text: val.name }));
        });

    });
};

$('#addartist').click(function(){

    $('#aform').jqxWindow({ width: '50%', maxWidth: '50%', minWidth: '50%', height:300, isModal: true, modalOpacity: 0.3,showCloseButton: false,cancelButton:$('#cancelartist')});
    $("#aform").css('display', 'inline');


});

$('#addgenre').click(function(){

    $('#genreformdiv').jqxWindow({ width: '50%', maxWidth: '50%', minWidth: '50%', height:300, isModal: true, modalOpacity: 0.3,showCloseButton: false,cancelButton:$('#cancelgenre')});
    $("#genreformdiv").css('display', 'inline');


});

$('#addalbums').click(function(){

    $('#albumform').jqxWindow({ width: '50%', maxWidth: '50%', minWidth: '50%', height:300, isModal: true, modalOpacity: 0.3,showCloseButton: false,cancelButton:$('#cancelalbum')});
    $("#albumform").css('display', 'inline');
    var $select = $('#albyear');
    $select.empty();
    var yr =(new Date).getFullYear();
    for(var i = yr; i>1980;i--)
    {
        $select.append($('<option />', { value: i, text: i }));
    }

});

$('#genreform').submit(function(e){
    e.preventDefault();
    showPleaseWait();
    var formData = new FormData($('#genreform')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'addgenre.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        if(xhr==1) {
            msgboxshow('Album Has Been Added', 3000);
            hidePleaseWait();
            $('#cancelgenre').click();
            getgenre();
        }else {
            hidePleaseWait();
            msgboxshow('The following error(s) occured <br/>' + xhr,5000);
        }

    })
});

$('#addalbum').submit(function(e){
    e.preventDefault();
    showPleaseWait();
    var formData = new FormData($('#addalbum')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'uploadalbum.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        if(xhr==1) {
            msgboxshow('Album Has Been Added', 3000);
            hidePleaseWait();
            $('#cancelalbum').click();
            getalbums();
        }else {
            hidePleaseWait();
            msgboxshow('The following error(s) occured <br/>' + xhr,5000);
        }

    })
});

$('#artistform').submit(function(e){
    e.preventDefault();
    showPleaseWait();
    var formData = new FormData($('#artistform')[0]);
    var ajaxRequest = $.ajax({
        type: "POST",
        url: 'uploadartist.php',
        contentType: false,
        processData: false,
        data:formData

    });
    ajaxRequest.done(function (xhr, textStatus) {
        if(xhr==1) {
            msgboxshow('Artist Has Been Added', 3000);
            hidePleaseWait();
            $('#cancelartist').click();
            getartist();
        }else {
            hidePleaseWait();
            msgboxshow('The following error(s) occured <br/>' + xhr,5000);
        }

    })
});

$('#songform').submit(function(e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    showPleaseWait();
    $.ajax({
        url: "uploadsong.php",
        type: "POST",
        data: formData,
        async: false,
        success: function (msg) {
            hidePleaseWait();
            console.log(msg);
            //$('#sp').removeAttribute('disabled');
            if (msg)
            {
                //msgboxshow("Your Pledge Has been Submitted Successfully",3000);
                alert(msg);
            }
            else
            {
                msgboxshow("Sorry Something Went Wrong try again",3000);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

function msgboxshow(msg, dur) {
    $('#msgtext').html(msg);
    $('.msgbox').show(500).delay(dur).hide(500);
}

function showPleaseWait() {
    var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false role="dialog">\
        <div class="modal-dialog">\
            <div class="modal-content">\
                <div class="modal-header">\
                    <h4 class="modal-title" style="text-align:center">Please wait...</h4>\
                </div>\
                <div class="modal-body">\
                    <div class="progress">\
                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                      aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                      </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>';
    $(document.body).append(modalLoading);
    $("#pleaseWaitDialog").modal("show");
};

function hidePleaseWait() {
    $("#pleaseWaitDialog").modal("hide");
};