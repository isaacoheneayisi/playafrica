/**
 * Created by Kofi Essel on 1/29/2016.
 */
(function() {
    var app = angular.module('starter.controllers', []);
    var url ='';
    var redSlider;
    var blueSlider;
    var curl='';

    app.controller('Malbum',['$http','$scope', function ($http,$scope){
        $scope.albums = [];

        $http.post(url+'/stribe/getalbums.php').success(function(data){
            $scope.albums = data;
            console.log(data);
        });

    }]);

    app.controller('mSession', ['$cookieStore', function($cookieStore) {
        // Put cookie
        $scope.putSession = function(phone){
            $cookieStore.put('mid',phone);
        };

        // Get cookie
        var favoriteCookie = $cookieStore.get('myFavorite');
        // Removing a cookie
        $cookieStore.remove('myFavorite');
    }]);

    app.controller('Martist',['$http','$scope', function ($http,$scope){
        $scope.artists = [];

        $http.post(url+'/stribe/getartist.php').success(function(data){
            $scope.artists = data;
            console.log(data);
        });


    }]);

    app.controller('SubmitCardInfo',['$http','$scope',function($http,$scope){

        $scope.p ={};
        $scope.processCard = function() {
            $scope.showPleaseWait();
        console.log($scope.p);
            // Posting data to php file
            console.log("here");
            $http.post(url+'/stribe/visamasterpayment.php?customerFirstName=' + $scope.p.customerFirstName + '&amount=' + $scope.p.amount + '&customerLastName=' + $scope.p.customerLastName + '&customerEmail=' + $scope.p.customerEmail + '&customerPhoneNumber=' + $scope.p.customerPhoneNumber)
                .success(function (data) {
                    $scope.hidePleaseWait();
                    $scope.hideVisaPayment();
                    window.open(data, '_blank');
                    console.log(data);
                    if (data.errors) {

                    } else {

                    }
                });
        };
    }]);

    app.controller('RegisterUser',['$scope','$http',function($scope,$http) {


        $scope.reg = {};
        $scope.registeru = function () {
            $scope.showPleaseWait();
            console.log($scope.reg);
            // Posting data to php file

            $http.post(url+'/stribe/register.php?name=' + $scope.reg.name + '&phone=' + $scope.reg.phone + '&password=' + $scope.reg.password)
                .success(function (data) {

                    $scope.hidePleaseWait();
                    console.log(data);
                    if (data == '1') {
                        window.localStorage.setItem('mid', $scope.reg.phone);
                        //$cookieStore.put('mid',$scope.reg.phone)
                        $scope.hideRegisterDialog();
                        //showAlert("User Registration","Registration Successful")

                    } else {
                        //showAlert("User Registration","Registration UnSuccessful")
                    }


                });

            /*$http({
             method  : 'POST',
             url     : 'http://scoopms.com/stribe/register.php',
             data    : $scope.reg
             })
             .success(function(data) {
             $ionicLoading.hide();
             console.log(data);
             if (data == '1') {
             window.localStorage.setItem('mid',$scope.reg.phone)
             //$cookieStore.put('mid',$scope.reg.phone)
             $scope.modalregister.hide();
             showAlert("User Registration","Registration Successful")

             } else {
             showAlert("User Registration","Registration UnSuccessful")
             }
             });*/
        };

    }]);


    app.controller('LoginUser',['$http','$scope',function($http,$scope){
        $scope.lin ={};


        $scope.login = function() {
            $scope.showPleaseWait();
            var pn = $scope.lin.phone;
            console.log($scope.lin);
            // Posting data to php file

            $http.post(url+'/stribe/login.php?phone='+pn +'&upass='+ $scope.lin.upass)
                .success(function(data){

                    $scope.hidePleaseWait();
                    console.log(data);
                    if (data == '1') {
                        window.localStorage.setItem('mid',pn);
                        $scope.showLErr = false;
                        $scope.hideLoginDialog();

                    } else {
                        $scope.showLErr = true;
                        $scope.alert = {msg: ' Invalid Username and Password'};
                        //showAlert("Login Status","Login Failed")
                    }

                });

            /*$http({
                method  : 'POST',
                url     : 'http://localhost/soundtrybegh/login.php',
                data    : {phone:$scope.lin.phone,upass:$scope.lin.upass} //forms user object

            })
                .success(function(data) {
                    $ionicLoading.hide();
                    console.log(data);
                    if (data == '1') {
                        window.localStorage.setItem('mid',pn);
                        $scope.modallogin.hide();

                    } else {
                        showAlert("Login Status","Login Failed")
                    }
                });*/
        };

        var showAlert = function(title,msg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });

            alertPopup.then(function(res) {
                console.log('then');
            });
        };
    }]);

    app.controller('ConfirmNumber',['$http','$scope',function($http,$scope){
        $scope.lin ={};

        $scope.confirmCode = function() {
            $scope.showPleaseWait();
            var pn = window.localStorage.getItem('mid');
            console.log($scope.c);
            // Posting data to php file

            $http.post(url+'/stribe/confirmnumber.php?phone='+pn +'&code='+ $scope.c.code)
                .success(function(data){

                    $scope.hidePleaseWait();
                    console.log(data);
                    if (data == '1') {
                        window.localStorage.setItem('mid',pn);
                        $scope.showCErr = false;
                        $scope.hideLoginDialog();

                    } else {
                        $scope.showCErr = true;
                        $scope.alertc = {msg: ' Invalid Confirmation Code'};
                        //showAlert("Login Status","Login Failed")
                    }

                });
        };

        var showAlert = function(title,msg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });

            alertPopup.then(function(res) {
                console.log('then');
            });
        };
    }]);

    app.controller('MobileMoneyCtrl',function($scope,$http){
        $scope.data = {};
        $scope.processAirtel = function() {
            $scope.showPleaseWait();


            // An elaborate, custom popup
            $http.post(url+'/stribe/airtelmm.php?phone='+$scope.data.phone+'&amount='+$scope.data.amount,{timeout:120000}).success(function(data){
                $scope.hidePleaseWait();
                if(data=='1')
                {
                    $scope.hideAirtelPayment();
                    alert("Transaction Successful");

                }else{

                    alert("Transaction Failed");

                }
                console.log(data);
            });


        }
    });

    app.controller('PopupCtrl',function($scope, $ionicPopup, $timeout) {

// Triggered on a button click, or some other target
        $scope.showPopup = function() {
            $scope.data = {};

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: '<input type="password" ng-model="data.wifi">',
                title: 'Enter Wi-Fi Password',
                subTitle: 'Please use normal things',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.wifi) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.data.wifi;
                            }
                        }
                    }
                ]
            });

            myPopup.then(function(res) {
                console.log('Tapped!', res);
            });

            $timeout(function() {
                myPopup.close(); //close the popup after 3 seconds for some reason
            }, 3000);
        };

        // A confirm dialog
        $scope.showConfirm = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Consume Ice Cream',
                template: 'Are you sure you want to eat this ice cream?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        };

        // An alert dialog
        $scope.showAlert = function(title,msg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });

            alertPopup.then(function(res) {
                console.log('Thank you for not eating my delicious ice cream cone');
            });
        };
    });

    app.controller('AlbumSongs',['$http','$scope','$routeParams', function ($http,$scope,$routeParams) {
        var sposition = 0;


        $scope.songs= [];
        $http.post(url+'/stribe/getsongs.php?albumid='+ $routeParams.albumid).success(function(data){
            $scope.songs = data;
            console.log(data);
        });
    }]);

    var hascrd = false;
    app.controller('MainCtrl',['$http','$routeParams','$scope','$interval',function($http,$routeParams,$scope,$interval) {



        $scope.trackreached = 0;
        $scope.songs= [];
        $scope.loadSongs = function() {
            $http.post(url+'/stribe/getsongs.php?artistid=' + $routeParams.artistid, {timeout: 120000}).success(function (data) {
                $scope.songs = data;
                console.log(data);

            });
        };

        var hasCredit = function(){
            $http.post(url+'/stribe/checks.php?accbal=0&phone='+ window.localStorage.getItem('mid') + '&act=play').success(function(data){
                // var p = document.getElementById('btnpl');
                if (data=='1'){
                    hascrd = true;
                }else{
                    if (window.localStorage.getItem('tt')>2){
                        hascrd = false;
                        $scope.showPayOptions();
                    }else{
                        hascrd =true
                    }

                }
                console.log(data);
            });
        };

        var testTimes = function(){
            var i = 0;
            if(window.localStorage.getItem('tt'))
            {
                i = window.localStorage.getItem('tt');
                i++;
                window.localStorage.setItem('tt',i);
            }else{
                window.localStorage.setItem('tt',0);
            }
            return window.localStorage.getItem('tt');
        };

        $scope.bimg = '';
        var sid=0;
        var oldsid = 0;

        $scope.setartist = function(aname,stitle,imgsrc,songid,fileurl){

            if(testTimes()>2){
                if( window.localStorage.getItem('mid'))
                {

                }else{
                    $scope.showRegisterDialog();
                    //$scope.modalregister.show();
                    hascrd = false;
                    return;
                }
            }else{

            }

            hasCredit();
            playingid = songid;

            if(oldsid == songid && $scope.isPlaying)
            {
                //$('#btnpl').removeClass('fa-pause').addClass('fa-play');
                document.getElementById('btnpl').style.display = 'inline';
                document.getElementById('btnpa').style.display = 'none';
                console.log('pause music')
            }else
            {
                $interval($scope.toggleButtons,700);

                document.getElementById('abutton').innerHTML= aname;
                document.getElementById('songtitle').innerHTML = stitle;
                sid= songid;
                bimg = imgsrc;
                curl = fileurl;

                document.getElementById('sideArtistName').innerHTML= aname;
                document.getElementById('imgbottom').src = imgsrc;
                document.getElementById('sideImg').src = imgsrc;



                document.getElementById('imgr' + sid).style.display = 'none';
                document.getElementById('img'+ sid).style.display = 'none';
                //document.getElementById('sloading').style.display = 'inline';
                document.getElementById('imgp'+ sid).style.display = 'none';
                //document.getElementById('img'+ sid).src = '../img/loading.gif';
                document.getElementById('btnpl').style.display = 'none';
                document.getElementById('btnpa').style.display = 'inline';
                //$('#btnpl').removeClass('fa-play').addClass('fa-pause');


                if(oldsid > 0){
                    document.getElementById('img'+ oldsid).style.display = 'inline';

                    document.getElementById('imgp'+ oldsid).style.display = 'none';
                    document.getElementById('imgr' + oldsid).style.display = 'none';
                    // document.getElementById('img'+ oldsid).src = '../img/play.png';
                    //restore preious playing settings
                }

                oldsid = sid;
            }




        };


        $scope.$on('track:progress', function(event, data) {
            //do your stuff here
            //document.getElementById('pg').style.width = data + '%';
            blueSlider.setValue(data);
            newtm = data;
            //document.getElementById('pg'+sid).style.width = data + '%';
            //var seekb = angular.element.find('input')[0];
            //seekb.value = data;
            console.log(data);

            if($scope.isPlaying)
            {
                console.log(hascrd);
                if(hascrd == false){
                    console.log(hascrd);
                    $('#btnpl').click();
                }

                console.log('its playing');
            }else{
                //document.getElementById('img'+ sid).src = '../img/play.png';
            }
            if($scope.progress >= 100){
                document.getElementById('img'+ playingid).style.display = 'inline';

                document.getElementById('imgp'+ playingid).style.display = 'none';
                document.getElementById('imgr' + playingid).style.display = 'none';
                //document.getElementById('img'+ sid).src = '../img/play.png';
                //$('#btnpl').toggleClass('fa-play fa-pause');

            }


        });


        var oldtm=0;
        var newtm=0;
        $scope.toggleButtons = function(){

            var pn = $scope.isPlaying;
            if(pn){
                $scope.showPause();
                $('#btnloading').removeClass('fa-circle-o-notch');
                //document.getElementById('btnloading').style.display = 'none';
                console.log('am playing');
            }else{
                $scope.showPlay();
                $('#btnloading').removeClass('fa-circle-o-notch');
                //document.getElementById('btnloading').style.display = 'none';
                console.log('am paused');
            }

            if(oldtm==newtm && pn){
                $('#btnloading').addClass('fa-circle-o-notch');
                console.log('am buffering oldtm='+oldtm + ' newtm='+newtm);
            }
            oldtm = newtm
        };

        $scope.$on('music:isPlaying', function(event, data) {
            //do your stuff here
            try {
                if (data) {


                    document.getElementById('imgr' + playingid).style.display = 'none';
                    document.getElementById('img' + playingid).style.display = 'none';

                    document.getElementById('imgp' + playingid).style.display = 'inline';
                    //document.getElementById('img'+ sid).src = '../img/pause.png';

                    //$('#btnpl').removeClass('fa-play').addClass('fa-pause');
                    document.getElementById('btnpl').style.display = 'none';
                    document.getElementById('btnpa').style.display = 'inline';
                    console.log('its playing');

                }
                else {
                    document.getElementById('imgp' + playingid).style.display = 'none';
                    document.getElementById('imgr' + playingid).style.display = 'inline';
                    document.getElementById('sloading').style.display = 'inline';

                    document.getElementById('btnpl').style.display = 'inline';
                    document.getElementById('btnpa').style.display = 'none';
                    //$('#btnpl').removeClass('fa-pause').addClass('fa-play');
                    console.log('music paused');

                    if ($scope.progress >= 100) {
                        document.getElementById('img' + sid).style.display = 'inline';
                        document.getElementById('imgr' + sid).style.display = 'none';
                        document.getElementById('sloading').style.display = 'none';

                        document.getElementById('imgp' + sid).style.display = 'none';
                        //document.getElementById('img'+ sid).src = '../img/play.png';
                        //$('#btnpl').toggleClass('fa-play fa-pause');
                        document.getElementById('btnpa').style.display = 'none';
                        document.getElementById('btnpl').style.display = 'inline';
                    }
                }
            }catch(err){

            }

            console.log(data);
        });
        var i=11;
        $scope.loadmore = function(){
            if($scope.songs.length<9) return;
            if ($scope.loadbusy) return;
            $scope.loadbusy = true;

            $http.post(url+'/stribe/getsongs.php?artistid='+ $routeParams.artistid + '&offset='+ i).success(function(items) {
                if (items == null) {

                    for(var j=0; j<items.length; j++)
                    {

                        $scope.songs.push(items[j]);

                    }
                }
                console.log(i);
                console.log(items);
                $scope.loadbusy=false;
            });
            i+=10;
        };

        $scope.initiateDownload = function(fileurl){
            $scope.showPleaseWait();
            $http.post(url+'/stribe/checks.php?accbal=0&phone='+ window.localStorage.getItem('mid') + '&act=download').success(function(data){
                // var p = document.getElementById('btnpl');
                $scope.hidePleaseWait();
                if (data=='1'){
                    var a = document.createElement('a');
                    a.href=fileurl;
                    a.target = '_blank';
                    a.download = 'soundtrybe';
                    document.body.appendChild(a);
                    a.click();
                }else{
                    $scope.showPayOptions();

                }
                console.log(data);
            });
        };

        angular.element(document).ready(function () {

            $scope.loadSongs();

        });


       /* var playingid =0;
            var ispaused = false;
            $scope.currentPercentage =0;
            $scope.pausemusic = function(sid){
                ispaused= true;
            };





        var i = 11;
              $scope.loadMoreData = function() {



                $http.post('https://www.scoopms.com/stribe/getsongs.php?artistid='+ $stateParams.artistid + '&offset='+ i).success(function(items) {
                    if (items == null) {
                        alert(items);
                        for(var j=0; j<items.length; j++)
                      {

                              $scope.songs.push(items[j]);

                      }
                    }
                    console.log(i);
                  console.log(items);
                   // console.log($scope.songs);
                  $scope.$broadcast('scroll.infiniteScrollComplete');
                });
                  i+=10;
              };




        function hasStoped(){

        }

        $scope.$on('currentTrack:position', function(event, data) {
            //do your stuff here
           // console.log(data + ' position');
        });
        var li = 0;
        angular.element(document).ready(function () {

            li++;
            console.log(li);

        });*/



    }]);

    app.controller('mctr', function ($scope,$http) {

        $scope.curl ='';

        $(function () {
            var pleaseWait = $('#pleaseWaitDialog');
            var loginDialog = $('#loginDialog');
            var registerDialog = $('#registerDialog');
            var payOptDialog = $('#payOptions');
            var payCards = $('#payCards');
            var visaPayment = $('#visaPayment');
            var airtelPayment = $('#airtelPayment');

            $scope.showAirtelPayment = function() {
                airtelPayment.modal('show');
            };

            $scope.hideAirtelPayment = function () {
                airtelPayment.modal('hide');
            };

            $scope.showVisaPayment = function() {
                visaPayment.modal('show');
            };

            $scope.hideVisaPayment = function () {
                visaPayment.modal('hide');
            };

            $scope.showPayCards = function() {
                payCards.modal('show');
            };

            $scope.hidePayCards = function () {
                payCards.modal('hide');
            };

            $scope.showPayOptions = function() {
                payOptDialog.modal('show');
            };

            $scope.hidePayOptions = function () {
                payOptDialog.modal('hide');
            };

            $scope.showPleaseWait = function() {
                pleaseWait.modal('show');
            };

            $scope.hidePleaseWait = function () {
                pleaseWait.modal('hide');
            };

            $scope.showLoginDialog = function() {
                loginDialog.modal('show');
            };

            $scope.hideLoginDialog = function () {
                loginDialog.modal('hide');
            };

            $scope.showRegisterDialog = function() {
                registerDialog.modal('show');
            };

            $scope.hideRegisterDialog = function () {
                registerDialog.modal('hide');
            };


            $scope.closeAlert = function() {
                $scope.showLErr = false;
            };


            redSlider = new Seekbar.Seekbar({
                renderTo: "#seekbar-container-vertical-red",
                minValue: 0, maxValue: 100,
                valueListener: function (value) {
                    this.value = Math.round(value);
                    updateColorPreview();
                },
                thumbColor: '#BBff0000',
                negativeColor: '#ff0000',
                positiveColor: '#CCC',
                value:0
            });

            blueSlider = new Seekbar.Seekbar({
                renderTo: "#seekbar-container-vertical-blue",
                minValue: 1, maxValue: 100,
                valueListener: function (value) {
                    this.value = Math.round(value);
                    updateColorPreview();
                },
                thumbColor: '#006699',
                negativeColor: '#006699',
                positiveColor: '#CCC',
                value: 0
            });

            $scope.showPlay = function(){
                document.getElementById('btnpl').style.display = 'inline';
                document.getElementById('btnpa').style.display = 'none';
            };

            $scope.showPause = function(){
                document.getElementById('btnpl').style.display = 'none';
                document.getElementById('btnpa').style.display = 'inline';
            };

            $scope.initiateDownloadBottom = function(){
                if(curl.length < 1){
                    return;
                }
                $scope.showPleaseWait();
                $http.post(url+'/stribe/checks.php?accbal=0&phone='+ window.localStorage.getItem('mid') + '&act=download').success(function(data){
                    // var p = document.getElementById('btnpl');
                    $scope.hidePleaseWait();
                    saveAs(curl);
//                    if (data=='1'){
//                        var a = document.createElement('a');
//                        a.href=curl;
//                        a.target = '_blank';
//                        a.download = 'soundtrybe';
//                        document.body.appendChild(a);
//                        a.click();
//                    }else{
//                        $scope.showPayOptions();
//
//                    }
                    console.log(data);
                });
            };

            function saveAs(url) {
                var filename = url.substring(url.lastIndexOf("/") + 1).split("?")[0];
                var xhr = new XMLHttpRequest();
                xhr.responseType = 'blob';
                xhr.onload = function() {
                    var a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhr.response);
                    a.download = filename;
                    a.target = '_self';
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                    delete a;
                };
                xhr.open('GET', url);
                xhr.send();
            }

            $scope.toggleSideMenu = function(){

                    $("#wrapper").toggleClass("toggled");

            }

        });


    });

    app.directive('modal', function () {
        return {
            template: '<div class="modal fade">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
                '</div>' +
                '<div class="modal-body" ng-transclude></div>' +
                '</div>' +
                '</div>' +
                '</div>',
            restrict: 'E',
            transclude: true,
            replace:true,
            scope:true,
            link: function postLink(scope, element, attrs) {
                scope.title = attrs.title;

                scope.$watch(attrs.visible, function(value){
                    if(value == true)
                        $(element).modal('show');
                    else
                        $(element).modal('hide');
                });

                $(element).on('shown.bs.modal', function(){
                    scope.$apply(function(){
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function(){
                    scope.$apply(function(){
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });

})();

