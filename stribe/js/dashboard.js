$(document).ready(function(){
    $('#startdate').datetimepicker({
        format: "DD-MM-YYYY",
        showTodayButton: true
    });
    $('#enddate').datetimepicker({
        format: "DD-MM-YYYY",
        showTodayButton: true
    });
});

function getReports() {

    $.ajax({
        url: 'getreports.php',
        data: {searchkey:''},
        type: 'post',
        success: function (output) {
            showdata(output);
        }
    });
}

function showdata(data) {
    source = {
        datatype: "json",
        datafields: [
            { name: 'id',type:'number' },
            { name: 'songname',type:'string' },
            { name: 'artist',type:'string' },
            { name: 'downloaded',type:'number'},
            { name: 'played',type:'number'}

        ],
        localdata: data
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
        { contentType: 'application/xml; charset=utf-8' }
    );

    $("#jqxgrid").jqxGrid(
        {
            theme: 'office',
            width: 700,
            columnsheight: 40,
            source: dataAdapter,
            columnsresize: true,
            pageable: true,
            pagesizeoptions: ['50', '100', '150', '200', '300'],
            pagesize: '50',
            autoheight: true,
            rendergridrows: function (args) {
                return args.data;
            },
            columns: [
                { text: 'Id', datafield: 'id', width: 20, hidden: true },
                { text: 'Song Title', datafield: 'songname', width: 250},
                { text: 'Artist Name', datafield: 'artist', width: 250},
                { text: 'Listerns', datafield: 'played', width: 100 },
                { text: 'Downloads', datafield: 'downloaded', width: 100 }
            ]
        });
}

function showdataRadio(data) {
    source = {
        datatype: "json",
        datafields: [
            { name: 'id',type:'number' },
            { name: 'title',type:'string' },
            { name: 'listeners',type:'number' },
            { name: 'filewhen',type:'string'}

        ],
        localdata: data
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
        { contentType: 'application/xml; charset=utf-8' }
    );

    $("#jqxgrid").jqxGrid(
        {
            theme: 'office',
            width: 700,
            columnsheight: 40,
            source: dataAdapter,
            columnsresize: true,
            pageable: true,
            pagesizeoptions: ['50', '100', '150', '200', '300'],
            pagesize: '50',
            autoheight: true,
            rendergridrows: function (args) {
                return args.data;
            },
            columns: [
                { text: 'Id', datafield: 'id', width: 20, hidden: true },
                { text: 'Radio Title', datafield: 'title', width: 350},
                { text: 'Number of Listeners', datafield: 'listeners', width: 100},
                { text: 'File Date', datafield: 'filewhen', width: 250 }
            ]
        });
}

$("#pdfExport").click(function () {
    $("#jqxgrid").jqxGrid('exportdata', 'pdf', 'jqxGrid');
});

$("#xlsExport").click(function () {
    $("#jqxgrid").jqxGrid('exportdata', 'xls', 'jqxGrid');
});

$('#reportform').submit(function(e){
    e.preventDefault();
    var sval = $('#reportname').val();
    var formData = new FormData($(this)[0]);

    $.ajax({
        url: "getreports.php",
        type: "POST",
        data: formData,
        async: false,
        success: function (msg) {
            console.log(msg);
            //$('#sp').removeAttribute('disabled');
            if (msg)
            {
                //msgboxshow("Your Pledge Has been Submitted Successfully",3000);
                if(sval == 'mscstat'){
                    showdata(msg);
                }else{
                    showdataRadio(msg);
                }

                $('#exportdiv').show();
            }
            else
            {
                msgboxshow("Sorry Something Went Wrong try again",3000);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$('#registeruser').submit(function(e){
    if($('#pword').val()==$('#cpword')){

    }else{
        msgboxshow('Password Mismatch',3000);
        return;
    }
    $('#registerModal').modal('hide');
    e.preventDefault();
    var formData = new FormData($(this)[0]);

    $.ajax({
        url: "account.php",
        type: "POST",
        data: formData,
        async: false,
        success: function (msg) {
            console.log(msg);
            //$('#sp').removeAttribute('disabled');
            if (msg)
            {
                $('#registerModal').modal('show');
                msgboxshow("Account Created Successfully",3000);
            }
            else
            {
                $('#registerModal').modal('show');
                msgboxshow("Sorry Something Went Wrong try again",3000);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

function msgboxshow(msg, dur) {
    $('#msgtext').html(msg);
    $('.msgbox').show(500).delay(dur).hide(500);
}