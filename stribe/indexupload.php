<?php
include('mfunc.php');
sec_session_start();
if(! isset($_SESSION['uname']))
{
    header('Location:index.php');
    return;
}
$connect = mysql_connect(HOSTNAME,USERNAME,PASSWORD) or die('Could not connect to db Server' . mysql_error());
$bool = mysql_select_db(DATABASE,$connect);

if($bool)
{

    getsongs();
}
function getsongs()
{
    global $connect;
    $query = <<<EOF
   		SELECT * FROM tblartist
EOF;

    $result = mysql_query($query, $connect) or die("SQL Error 1: " . mysql_error());
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>SoundTrybeGH</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!--    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <!-- <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SoundTrybeGH</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="indexupload.php">Home</a></li>
                <li><a href="allsongs.php">All Songs</a></li>
                <li><a href="dashboard.php">Reports</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">

    <div class="songsform">

        <h1>Add Songs</h1>
        <br/>
        <form role="form" id="songform" method="post" name="sform" enctype="multipart/form-data">
            <div class="input-group">

                <span class="input-group-addon">Track Title</span>
                <input type="text" class="form-control" name="stitle" id="stitle" placeholder="Song Title">

            </div>
            <br/>

            <div class="input-group">
                <span class="input-group-addon" style="cursor: pointer" id="addartist">Add Artist</span>
                <select id="sartist" name="sartist" class="form-control" aria-describedby="basic-addon1"></select>

            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon" id="addalbums" style="cursor: pointer">Add Album</span>
                <select id="salbum" name="salbum" class="form-control" aria-describedby="basic-addon1"></select>

            </div>

            <br/>

            <div class="input-group">
                <span class="input-group-addon" style="cursor: pointer" id="addgenre">Add Genre</span>
                <select id="sgenre" name="sgenre" class="form-control"></select>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon">Track File</span>

                <input name="sfile" type="file" id="sfile" placeholder="Music File">

            </div>
            <br/>

            <div class="input-group">
                <span class="input-group-addon">Track Image</span>
                <input type="file" name="simg" id="simg" placeholder="Track Image">
            </div>

            <br/>

            <div class="input-group">
                <span class="input-group-addon">CRBT</span>
                <input type="text" name="crbt" id="crbt" placeholder="CRBT Code">
            </div>

            <br/>

            <div class="form-group" style="float:right">

                    <button type="submit" class="btn btn-primary">Upload</button>

            </div>
        </form>
    </div>

</div><!-- /.container -->

<div class="artistform" id="aform" style="display:none">
    <div style="display:none" ></div>
    <div>
        <h4 style="text-align: center">Add Artist</h4>
        <form role="form" id="artistform" method="post" action="uploadartist.php" name="artistform" enctype="multipart/form-data">
            <div class="input-group">
                <span class="input-group-addon">Artist Name</span>
                <input type="text" class="form-control" name="aname" id="aname" placeholder="Artist Name" required>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon">Country</span>
                <select id="acountry" name="acountry" class="form-control">
                    <option value="Ghana">Ghana</option>
                    <option value="Nigeria">Nigeria</option>
                </select>
            </div>
            <br/>
            <div class="iput-group">
                <span class="input-group-addon">Artist Image</span>
                <input type="file" name="aimg" id="aimg" placeholder="Artiist Image" required/>
            </div>
            <br/>
            <div class="input-group" style="float:right">
                <button type="button" class="btn btn-danger" id="cancelartist">Cancel</button>
                <button type="submit" class="btn btn-primary">Upload</button>

            </div>
        </form>
    </div>
</div>

<div class="artistform" id="albumform" style="display:none">
    <div style="display:none" ></div>
    <div>
        <h4 style="text-align: center">Add Album</h4>
        <form role="form" id="addalbum" method="post" action="uploadalbum.php" name="albumform" enctype="multipart/form-data">
            <div class="input-group">
                <span class="input-group-addon">Album Name</span>
                <input type="text" class="form-control" name="albname" id="albname" placeholder="Album Name" required>
            </div>
            <br/>
            <div class="input-group">
                <span class="input-group-addon">Album Year</span>
                <select id="albyear" name="albyear" class="form-control">

                </select>
            </div>
            <br/>
            <div class="iput-group">
                <span class="input-group-addon">Album Image</span>
                <input type="file" name="albimg" id="albimg" placeholder="Album Image" required/>
            </div>
            <br/>
            <div class="input-group" style="float:right">
                <button type="button" class="btn btn-danger" id="cancelalbum">Cancel</button>
                <button type="submit" class="btn btn-primary">Upload</button>
            </div>
        </form>
    </div>
</div>

<div class="artistform" id="genreformdiv" style="display:none">
    <div style="display:none" ></div>
    <div>
        <h4 style="text-align: center">Add Genre</h4>
        <form role="form" id="genreform" method="post" action="addgenre.php" name="addgenre" >
            <div class="input-group">
                <span class="input-group-addon">Genre Name</span>
                <input type="text" class="form-control" name="gname" id="gname" placeholder="Genre Name" required>
            </div>
            <br/>

            <br/>
            <div class="input-group" style="float:right">
                <button type="button" class="btn btn-danger" id="cancelgenre">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>

<div class="msgbox">
    <p id="msgtext"></p>
</div>



<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.pager.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.grouping.js"></script>
<script type="text/javascript" src="jqwidgets/jqxwindow.js"></script>
<script type="text/javascript" src="jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdatatable.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.aggregates.js"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<script src="js/indexScript.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
