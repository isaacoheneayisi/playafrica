<?php
$fn = $_GET['fn'];
$findx = strripos($fn,'/');
$track = substr($fn,$findx);

$track = 'songs/'.$track;
if (file_exists($track)) {
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control:");
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Type: audio/mpeg");
    header('Content-Length: ' . filesize($track));
    header('Content-Disposition: attachment; filename="'.$track.'"');
    header("Content-Transfer-Encoding: binary");
    header('Cache-Control: no-cache');
    readfile($track);
    exit;
} else {
    header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found', true, 404);
    echo "no file";
}
?>