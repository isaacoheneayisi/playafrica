<?php
/**
 * Created by PhpStorm.
 * User: Gideon Paitoo
 * Date: 2/29/2016
 * Time: 12:32 PM
 */
$phone = $_REQUEST['accphone'];

$phonenum = '';
if(strlen($phone)==10){
    $phonenum = '233'.substr($phone,1);
}else if(startsWith($phone,'233') && strlen($phone)==12){
    $phonenum =$phone;
}else if(strlen($phone)==13){
    if(startsWith(substr($phone,1),'233')){
        $phonenum = substr($phone,1);
    }else{
        echo 'invalid phone';
        return;

    }
}else{
    echo 'invalid phone';
    return;
}

if (!empty($phonenum) && $phonenum != "NA") {
    $smscID='';
}

    if(substr($phonenum, 0, 5) == "23324" || substr($phonenum, 0, 5) == "23354")
    {
        $smscID='MTN';
        $url_to_call = "http://107.6.149.82/subengine_call.php?shortcode=1735&msisdn={$phonenum}&req_text=spt&smscID={$smscID}";
    }
    else
    {
        if(substr($phonenum, 0, 5) == "23320" || substr($phonenum, 0, 5) == "23350") {
            $smscID='VODAFONE';
        }elseif(substr($phonenum, 0, 5) == "23326" || substr($phonenum, 0, 5) == "23356"){
            $smscID='AIRTEL';
        }elseif(substr($phonenum, 0, 5) == "23323"){
            $smscID = 'GLO';
        }else{
            $smscID='';
        }
        $url_to_call = "http://node01.mtechgh.com/subengine_call.php?shortcode=1735&msisdn={$phonenum}&req_text=spt&smscID={$smscID}";
    }


//$url='http://node01.mtechgh.com/subengine_call.php?shortcode=1735&msisdn='.$phone.'&req_text=spt&smscID={$smscID}"';

$xml_data ='';
echo $url_to_call.'<br/>';

//$URL = "http://api.infobip.com/api/v3/sendsms/xml";

$ch = curl_init($url_to_call);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(''));
//curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$output = curl_exec($ch);
echo $output;
curl_close($ch);

function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}