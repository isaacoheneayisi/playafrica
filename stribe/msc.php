<?php
include('mfunc.php');
global $mysqli;
$mysqli = new mysqli(HOSTNAME,USERNAME,PASSWORD,DATABASE) or die('Could not connect to db Server' . mysql_error());

$bool = false;
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}else{$bool = true;}

if($bool)
{
    $fn = $_REQUEST['fn'];
    $id = $_REQUEST['id'];
    if($fn=='artist'){
        getartist($id);
    }else if($fn=='artistupdate'){
        updateartist();
    }else if($fn=='album'){
        getalbum($id);
    }else if($fn=='albumupdate'){
        updatealbum();
    }

}
function getartist($id)
{
    $qd =<<<EOF
SELECT * FROM tblartist WHERE id=$id;
EOF;

    global $connect;
    global $mysqli;
    $Result = $mysqli->query($qd) or die($mysqli->error);
    while($row = $Result->fetch_object() ){
        $items[] = array(
            'id' => $row->id,
            'name' => $row->name,
            'fullname' => $row->fullname,
            'country' => $row->country,
            'img' => $row->img,

        );
    }
echo json_encode($items);

}

function updateartist()
{
    global $bool;
    global $mysqli;
    $errmsg='';
    $err = 0;
    $imgname = '';


    $urlimg = 'https://scoopms.com/stribe/stghimages/artist/';


    if (isset($_FILES["aimg"])) {
        $allowedExts = array("jpg", "png", "jpeg");
        //echo $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $fileName = $_FILES['aimg']['name'];
        $extension = substr($fileName, strrpos($fileName, '.') + 1); // getting the info about the image to get its extension

        /*if ((($_FILES["file"]["type"] == "video/mp4")|| ($_FILES["file"]["type"] == "audio/mp3")|| ($_FILES["file"]["type"] == "audio/wma")|| ($_FILES["file"]["type"] == "image/pjpeg")|| ($_FILES["file"]["type"] == "image/gif")|| ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 200000) && in_array($extension, $allowedExts))*/

        if (in_array($extension, $allowedExts)) {
            if ($_FILES["aimg"]["error"] > 0) {
                $errmsg = "Return Code: " . $_FILES["aimg"]["error"] . "<br />";
                $err++;
            } else {

                /* echo "Upload: " . $_FILES["aimg"]["name"] . "<br />";
                 echo "Type: " . $_FILES["aimg"]["type"] . "<br />";
                 echo "Size: " . ($_FILES["aimg"]["size"] / 1024) . " Kb<br />";
                 echo "Temp file: " . $_FILES["aimg"]["tmp_name"] . "<br />";*/

                if (file_exists("stghimages/artist" . $_FILES["aimg"]["name"])) {
                    $errmsg = $_FILES["aimg"]["name"] . " already exists. ";
                    $err++;
                } else {
                    $imgname = rand(10000000000, 99999999999) . $_FILES["aimg"]["name"];
                    move_uploaded_file($_FILES["aimg"]["tmp_name"], "stghimages/artist/" . $imgname);
                    //echo "Stored in: " . "stghimages/artist/" . $imgname;
                }
            }
        } else {
            $errmsg= "Invalid Image file";
            $err++;
        }
    }

    if ($bool) {
        $aname = $_POST['aname'];
        $acountry = $_POST['acountry'];
        $id = $_POST['id'];
        $img = $_POST['image'];

        $updateimgurl = '';
        if(strlen($imgname) > 0){
            $updateimgurl = $urlimg.$imgname;
        }else
        {
            $updateimgurl = $img;
        }

        if (true) {
            $insertsql = <<<EOF
            UPDATE tblartist SET name='$aname',country='$acountry',img='$updateimgurl' WHERE id=$id
EOF;

            $Result1 =  $mysqli->query($insertsql) or die($mysqli->error);
            if ($Result1 == 1) {
                echo true;
            } else {
                echo false;
            }
        }
        else{
            echo $errmsg;
        }

    }
}


function getalbum($id)
{
    $qd =<<<EOF
SELECT * FROM tblalbum WHERE id=$id;
EOF;

    global $connect;
    global $mysqli;
    $Result = $mysqli->query($qd) or die($mysqli->error);
    while($row = $Result->fetch_object() ){
        $items[] = array(
            'id' => $row->id,
            'title' => $row->title,
            'year' => $row->year,
            'img' => $row->img
        );
    }
    echo json_encode($items);

}

function updatealbum()
{
    global $bool;
    global $mysqli;
    $errmsg='';
    $err = 0;
    $imgname = '';


    $urlimg = 'https://scoopms.com/stribe/stghimages/artist/';


    if (isset($_FILES["albimg"])) {
        $allowedExts = array("jpg", "png", "jpeg");
        //echo $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
        $fileName = $_FILES['albimg']['name'];
        $extension = substr($fileName, strrpos($fileName, '.') + 1); // getting the info about the image to get its extension

        /*if ((($_FILES["file"]["type"] == "video/mp4")|| ($_FILES["file"]["type"] == "audio/mp3")|| ($_FILES["file"]["type"] == "audio/wma")|| ($_FILES["file"]["type"] == "image/pjpeg")|| ($_FILES["file"]["type"] == "image/gif")|| ($_FILES["file"]["type"] == "image/jpeg")) && ($_FILES["file"]["size"] < 200000) && in_array($extension, $allowedExts))*/

        if (in_array($extension, $allowedExts)) {
            if ($_FILES["albimg"]["error"] > 0) {
                $errmsg = "Return Code: " . $_FILES["albimg"]["error"] . "<br />";
                $err++;
            } else {

                /* echo "Upload: " . $_FILES["albimg"]["name"] . "<br />";
                 echo "Type: " . $_FILES["albimg"]["type"] . "<br />";
                 echo "Size: " . ($_FILES["albimg"]["size"] / 1024) . " Kb<br />";
                 echo "Temp file: " . $_FILES["albimg"]["tmp_name"] . "<br />";*/

                if (file_exists("stghimages/artist" . $_FILES["albimg"]["name"])) {
                    $errmsg = $_FILES["albimg"]["name"] . " already exists. ";
                    $err++;
                } else {
                    $imgname = rand(10000000000, 99999999999) . $_FILES["albimg"]["name"];
                    move_uploaded_file($_FILES["albimg"]["tmp_name"], "stghimages/artist/" . $imgname);
                    //echo "Stored in: " . "stghimages/artist/" . $imgname;
                }
            }
        } else {
            $errmsg= "Invalid Image file";
            $err++;
        }
    }

    if ($bool) {
        $albname = $_POST['albname'];
        $albyear = $_POST['albyear'];
        $id = $_POST['id'];
        $img = $_POST['image'];

        $updateimgurl = '';
        if(strlen($imgname) > 0){
            $updateimgurl = $urlimg.$imgname;
        }else
        {
            $updateimgurl = $img;
        }

        if (true) {
            $insertsql = <<<EOF
            UPDATE tblalbum SET title='$albname',year='$albyear',img='$updateimgurl' WHERE id=$id
EOF;

            $Result1 =  $mysqli->query($insertsql) or die($mysqli->error);
            if ($Result1 == 1) {
                echo true;
            } else {
                echo false;
            }
        }
        else{
            echo $errmsg;
        }

    }
}