<?php
include('mfunc.php');

$mysqli = new mysqli(HOSTNAME,USERNAME,PASSWORD,DATABASE) or die('Could not connect to db Server' . mysql_error());
$bool = false;
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}else{$bool = true;}

if($bool)
{
    $reportname= $_REQUEST['reportname'];




    if($reportname == 'mscstat'){
        $sdate = $_REQUEST['sdate'];
        $edate = $_REQUEST['edate'];

        $time = strtotime($sdate);
        $sdate = date('Ymd',$time);

        $time = strtotime($edate);
        $edate = date('Ymd',$time);

        getreport($sdate,$edate);
    }
    else if($reportname=='radstat'){
        $sdate = $_REQUEST['sdate'];
        $edate = $_REQUEST['edate'];

        $time = strtotime($sdate);
        $sdate = date('Ymd',$time);

        $time = strtotime($edate);
        $edate = date('Ymd',$time);
        $sdate = $sdate.'000000';
        $edate = $edate.'235900';
        getRadioStat($sdate,$edate);
    }

}
function getreport($sdate,$edate)
{

    $logwhen = date('Ymd');
    $qd =<<<EOF
    select songid as id,songtitle,artistname,sum(played) as played,sum(downloaded)as downloaded from tblsonglogs where $sdate <= logwhen and logwhen <= $edate GROUP BY songid
EOF;

    global $connect;
    global $mysqli;
    $Result = $mysqli->query($qd) or die(mysql_error());
    while($row = $Result->fetch_object() ){
        $item[] = array(
           'id' => $row->id,
            'songname' => $row->songtitle,
            'artist' => $row->artistname,
            'played' => $row->played,
            'downloaded' => $row->downloaded
        );
    }
echo json_encode($item);

}

function getRadioStat($sdate,$edate)
{
    $qd =<<<EOF
SELECT * FROM tblradiostat where $sdate <= trackwhen and trackwhen <= $edate
EOF;
    $title = '';
    global $connect;
    global $mysqli;
    $Result = $mysqli->query($qd) or die($mysqli->error);
    while($row = $Result->fetch_object() ){
        $items[] = array(
            'id' => $row->id,
            'title' => $row->title,
            'listeners' => $row->listeners,
            'filewhen' => formatdate($row->trackwhen)
        );
    }
    echo json_encode($items);
}

function formatdate($d){
    $y = substr($d,0,4);
    $m = substr($d,4,2);
    $da = substr($d,6,2);
    $h = substr($d,8,2);
    $mi = substr($d,10,2);
    $s = substr($d,12);
    return "{$da}/{$m}/{$y} {$h}:{$mi}:{$s}";
}