$(document).ready(function(){

   getsongs();
});


function getsongs() {

    $.ajax({
        url: 'getallsongs.php',
        data: {searchkey:''},
        type: 'post',
        success: function (output) {
            showdata(output);
        }
    });
}

$('#editartist').click(function(){
    $('#songModal').modal('hide');
    showPleaseWait();
    var id = $('#sartist').val();
    var data = '';
    $.ajax({
        url: 'msc.php',
        data: {fn:'artist',id:id},
        type: 'post',
        dataType:'json',
        success: function (output) {
            hidePleaseWait();
            $.each(output, function(index, element) {
                $('#aname').val(element.name);
                $('#acountry').val(element.country);
                $('#id').val(element.id);
                $('#aimage').val(element.img);
                });

            $('#artistModal').modal('show');
        }
    });

});

$('#editalbum').click(function(){
    $('#songModal').modal('hide');
    showPleaseWait();
    var id = $('#salbum').val();
    console.log(id);
    $.ajax({
        url: 'msc.php',
        data: {fn:'album',id:id},
        type: 'post',
        dataType:'json',
        success: function (output) {
            console.log(output);
            hidePleaseWait();
            var $select = $('#albyear');
            $select.empty();
            var yr =(new Date).getFullYear();
            for(var i = yr; i>1980;i--)
            {
                $select.append($('<option />', { value: i, text: i }));
            }
            $.each(output, function(index, element) {
                $('#albname').val(element.title);
                $('#albyear').val(element.year);
                $('#alid').val(element.id);
                $('#alimage').val(element.img);
            });

            $('#albumModal').modal('show');
        },
        error: function(err){
            console.log(err);
        }
    });

});

$('#editartistform').submit(function(e){
    $('#artistModal').modal('hide');
    e.preventDefault();
    showPleaseWait();
    var formData = new FormData($(this)[0]);
    $.ajax({
        url: "msc.php",
        type: "POST",
        data: formData,
        async: true,
        success: function (msg) {
            hidePleaseWait();
            console.log(msg);
            //$('#sp').removeAttribute('disabled');
            if (msg == 1)
            {
                $('#songModal').modal('show');
                msgboxshow("Update Submitted Successfully",3000);
            }
            else
            {
                $('#artistModal').modal('show');
                msgboxshow("Sorry Something Went Wrong try again",3000);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$('#editalbumform').submit(function(e){
    $('#albumModal').modal('hide');
    showPleaseWait();
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        url: "msc.php",
        type: "POST",
        data: formData,
        async: true,
        success: function (msg) {
            hidePleaseWait();
            console.log(msg);
            //$('#sp').removeAttribute('disabled');
            if (msg == 1)
            {
                $('#songModal').modal('show');
                msgboxshow("Update Submitted Successfully",3000);
            }
            else
            {
                $('#albumModal').modal('show');
                msgboxshow("Sorry Something Went Wrong try again",3000);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});


    function showdata(data) {
    source = {
        datatype: "json",
        datafields: [
            { name: 'id' },
            { name: 'title' },
            { name: 'crbtcode' },
            { name: 'name'},
            { name: 'genreid'},
            { name: 'albumid'},
            { name: 'artistid'},
            { name: 'country'},
            { name: 'url'},
            { name: 'img'},
            { name: 'playtime'}

        ],
        localdata: data
    };

    var dataAdapter = new $.jqx.dataAdapter(source,
        { contentType: 'application/xml; charset=utf-8' }
    );

    $("#jqxgrid").jqxGrid(
        {
            theme: 'office',
            width: 700,
            columnsheight: 40,
            source: dataAdapter,
            columnsresize: true,
            pageable: true,
            pagesizeoptions: ['15', '30', '45', '60', '100'],
            pagesize: '15',
            autoheight: true,
            rendergridrows: function (args) {
                return args.data;
            },
            columns: [
                { text: 'Id', datafield: 'id', width: 20, hidden: true },
                { text: 'Title', datafield: 'title', width: 200},
                { text: 'Artist Name', datafield: 'name', width: 200},
                { text: 'CRBT Code', datafield: 'crbtcode', width: 300 },

            ]
        });
}

$('#jqxgrid').on('rowclick', function (event) {
    var id = '';
    var title='';
    var crbtcode='';
    var genreid ='';
    var albumid = '';
    var artistid = '';
    var country = '';
    var fileurl = '';
    var imgurl = '';
    var playtime = '';

    var indx = event.args.rowindex;
    var data = $('#jqxgrid').jqxGrid('getrowdata', indx);

    id=data.id;
    title= data.title;
    crbtcode=data.crbtcode;
    genreid=data.genreid;
    albumid = data.albumid;
    artistid = data.artistid;
    country = data.country;
    fileurl = data.url;
    imgurl = data.img;
    playtime = data.playtime;

    $('#stitle').val(title);
    $('#crbt').val(crbtcode);
    $('#sartist').val(artistid);
    $('#salbum').val(albumid);
    $('#sgenre').val(genreid);
    $('#songid').val(id);
    $('#fileurl').val(fileurl);
    $('#imgurl').val(imgurl);
    $('#playtime').val(playtime);

    $('#songModal').modal('show');

});

$('#editsongform').submit(function(e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    showPleaseWait();
    $.ajax({
        url: "updatesongs.php",
        type: "POST",
        data: formData,
        async: false,
        success: function (msg) {
            hidePleaseWait();
            console.log(msg);
            //$('#sp').removeAttribute('disabled');
            if (msg == 1)
            {
                msgboxshow("Update Submitted Successfully",3000);
            }
            else
            {
                msgboxshow("Sorry Something Went Wrong try again",3000);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
});



function msgboxshow(msg, dur) {
    $('#msgtext').html(msg);
    $('.msgbox').show(500).delay(dur).hide(500);
}

function showPleaseWait() {
    var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false role="dialog">\
        <div class="modal-dialog">\
            <div class="modal-content">\
                <div class="modal-header">\
                    <h4 class="modal-title" style="text-align:center">Please wait...</h4>\
                </div>\
                <div class="modal-body">\
                    <div class="progress">\
                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                      aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                      </div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>';
    $(document.body).append(modalLoading);
    $("#pleaseWaitDialog").modal("show");
};

function hidePleaseWait() {
    $("#pleaseWaitDialog").modal("hide");
};