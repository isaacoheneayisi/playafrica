<?php
include_once('mfunc.php');
sec_session_start();
if(! isset($_SESSION['uname']))
{
    header('Location:index.php');
    return;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>SoundTrybeGH</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">

    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!--    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <!-- <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SoundTrybeGH</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="indexupload.php">Home</a></li>
                <li><a href="allsongs.php">All Songs</a></li>
                <li><a href="dashboard.php">Reports</a></li>
                <li>
                    <a href="#">Setup</a>
                    <ul>
                        <li>Create Account</li>
                        <li>Manage Accounts</li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="container">
    <div style="position: relative">
        <div class="dashrpt">
            <h3 class="text-center">Report Options</h3><br>
            <form class="form-inline" id="reportform">
                <div class="form-group">
                    <label for="reportname">Report Name</label>
                    <select id="reportname" name="reportname" class="form-control">
                        <option value="mscstat">Music Report</option>
                        <option value="radstat">Radio Report</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="startdate">Start Date</label>
                    <input type="text" name="sdate" class="form-control sdate" id="startdate" placeholder="Select Date" required>
                </div>
                <div class="form-group">
                    <label for="enddate">End Date</label>
                    <input type="text" name="edate" class="form-control" id="enddate" placeholder="Select Date" required>
                </div>
                <button type="submit" class="btn btn-primary">Get Report</button>
            </form>

            <div style="width:700px; margin:30px auto 0 auto; display:none" id="exportdiv">
            <div class="btn-group" role="group" style="float:right">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Save As
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#" id="pdfExport">PDF</a></li>
                    <li><a href="#" id="xlsExport">Excel</a></li>
                </ul>
            </div>

            <div id="jqxgrid" style="margin:50px auto 0 auto">

            </div>


        </div>
            </div>
        </div>




    <div id="registerModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 style="text-align: center">Create Account</h4>
                    <form role="form" id="registeruser" method="post" action="" name="registerform" enctype="multipart/form-data">
                        <input type="hidden" name="select" value="register"/>
                        <div class="input-group">
                            <span class="input-group-addon">Name</span>
                            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Full Name" required>
                        </div>
                        <br/>
                        <div class="input-group">
                            <span class="input-group-addon">Username</span>
                            <input type="text" class="form-control" name="uname" id="uname" placeholder="UserName" required/>

                        </div>
                        <br/>
                        <div class="input-group">
                            <span class="input-group-addon">Access Type</span>
                            <select id="reportname" name="access" class="form-control">
                                <option value="admin">Administrator</option>
                                <option value="content">Content</option>
                            </select>
                        </div>
                        <br/>
                        <div class="input-group">
                            <span class="input-group-addon">Password</span>
                            <input type="password" name="pword" id="pword" placeholder="Password" class="form-control"/>
                        </div>
                        <br/>
                        <div class="input-group">
                            <span class="input-group-addon">Confirm Password</span>
                            <input type="password" name="cpword" id="cpword" placeholder="Confirm Password" class="form-control"/>
                        </div>
                        <br/>
                        <div class="input-group" style="float:right">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="$('#songModal').modal('show');">Cancel</button>
                            <button type="submit" class="btn btn-primary">Submit</button>

                        </div>
                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
</div>


<div class="msgbox">
    <p id="msgtext"></p>
</div>



<script type="text/javascript" src="jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.js"></script>
<script type="text/javascript" src="jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.selection.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.filter.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.pager.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.grouping.js"></script>
<script type="text/javascript" src="jqwidgets/jqxwindow.js"></script>
<script type="text/javascript" src="jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdatatable.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.aggregates.js"></script>
<script type="text/javascript" src="jqwidgets/jqxdata.export.js"></script>
<script type="text/javascript" src="jqwidgets/jqxgrid.export.js"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->

<script src="js/bootstrap.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/dashboard.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>