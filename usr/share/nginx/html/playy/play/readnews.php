<?php
set_time_limit(0);
error_reporting(E_ERROR | E_PARSE);

$c = $_REQUEST['c'];
$cuurl = $_REQUEST['curl'];
if($c=='1'){
    header('LOCATION: getnews.php?nurl='.$cuurl);
}else{

}

$nurl = $_REQUEST['nurl'];
$result = '';
$options = array(
    CURLOPT_RETURNTRANSFER => true, // return web page
    CURLOPT_HEADER => false, // do not return headers
    CURLOPT_FOLLOWLOCATION => false, // follow redirects
    CURLOPT_USERAGENT => "browser", // who am i
    CURLOPT_AUTOREFERER => true, // set referer on redirect
    CURLOPT_CONNECTTIMEOUT => 920, // timeout on connect
    CURLOPT_TIMEOUT => 920, // timeout on response
    CURLOPT_MAXREDIRS => 500, // stop after 10 redirects
);
// Create a URL handle.
$ch = curl_init('http://tv3network.com/'.$cuurl);

// Tell curl what URL we want.
curl_setopt_array($ch, $options);

// Download the HTML from the URL.
$content = curl_exec($ch);

// Check to see if there were errors.
if(curl_errno($ch)) {
// We have an error. Show the error message.
    echo curl_error($ch);
}
else {
// No error. Save the page content.
    processcontent($content);
}

function processcontent($html)
{
    global $result;
    $dom = new DOMDocument();
    $dom->loadHTML($html);

    $xpath = new DOMXPath($dom);

    foreach ($xpath->evaluate('//div[@class="itemBody"]/node()') as $childNode) {
        $result .= $dom->saveHtml($childNode);
    }
}

function replace_img_src($img_tag) {
    $doc = new DOMDocument();
    $doc->loadHTML($img_tag);

    $tags = $doc->getElementsByTagName('img');
    foreach ($tags as $tag) {
        $old_src = $tag->getAttribute('src');
        $new_src_url = 'http://tv3network.com/'.$old_src;
        $tag->setAttribute('src', $new_src_url);
    }
    return utf8_decode($doc->saveHTML($doc->documentElement));
}

//echo replace_img_src($result);

?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TV3 News</title>
    <!-- Core CSS - Include with every page -->

<!--    <link href="http://tv3network.com/news/feed.html?type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0" />-->
    <link href="http://tv3network.com/news/feed.html?type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0" />
    <link href="http://tv3network.com//templates/noo_noonews/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="css/k2.css" type="text/css" />


    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />



    <script src="js/jquery-1.11.1.min.js"></script>

    <script src="js/bootstrap.min.js" type="text/javascript"></script>

    <style>
        body{
            background-color:#ffffff;background-image: none;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">TV3 News</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="getnews.php?nurl=news.html">Home</a></li>
                <li><a href="getnews.php?nurl=politics.html">Politics</a></li>
                <li><a href="getnews.php?nurl=business.html">Business</a></li>
                <li><a href="getnews.php?nurl=sports.html">Sports</a></li>
                <li><a href="getnews.php?nurl=entertainment-world.html">Entertainment</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<br/>
<br/>
<br/>
<div class="container">
<?php
echo replace_img_src($result)
?>
</div>
<script>
    $(document).ready(function(){

        setInterval(execExtra,4000);

        function execExtra(){
            $('.bt-social-share').css('display','none');
            $('.adsbygoogle').css('display','none');
        }

    });
</script>


</body>

</html>