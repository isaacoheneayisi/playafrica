// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
(function() {
    angular.module('starter', ['ngRoute','starter.controllers','infinite-scroll','yaru22.angular-timeago'])

        .config(function ($httpProvider) {
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
    })

        .run(['$rootScope','$location', '$routeParams', function($rootScope, $location, $routeParams) {
            $rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
                var pth = $location.path();
                if(pth=='/home/'){
                    $('#sidebar').show();
                    $('#bjax-target').addClass('padder-lg');
                    $('#seekbar-container-vertical-blue').css('margin','0');
                }else if(pth=='/playlist')
                {
                    $('#bjax-target').removeClass('padder-lg');
                    $('#sidebar').hide();
                    $('#seekbar-container-vertical-blue').css('margin','0 auto 0 auto');
                }else if(pth=='/radio')
                {
                    $('#bjax-target').removeClass('padder-lg');
                    $('#sidebar').hide();
                    $('#seekbar-container-vertical-blue').css('margin','0 auto 0 auto');

                }else if(pth=='/hitlist'){
                    $('#bjax-target').removeClass('padder-lg');
                    $('#sidebar').hide();
                    $('#seekbar-container-vertical-blue').css('margin','0 auto 0 auto');
                }
                else{
                    $('#bjax-target').addClass('padder-lg');
                    $('#seekbar-container-vertical-blue').css('margin','0');
                }
                //console.log(pth);
                // Get all URL parameter
                //console.log($routeParams);
            });
        }])
        /*.run(function ($rootScope) {
            $rootScope.$on('$routeChangeSuccess', function (e, current, pre) {
                var pth = current.$$route.originalPath;
                if(pth=='/home/'){
                    $('#sidebar').show();
                }else if(pth=='/playlist')
                {
                    $('#sidebar').hide();
                }
                console.log(pth);
            });
        })*/

        .config(function($routeProvider) {
            $routeProvider

                // route for the home page

                .when('/home/', {
                     templateUrl : 'templates/tmphome.html',
                     controller  : 'MainCtrl'
                })

                // route for the about page
                .when('/albums', {
                    templateUrl : 'templates/tmpalbums.html',
                    controller  : 'Malbum'
                })

                .when('/artist', {
                    templateUrl : 'templates/tmpartist.html',
                    controller  : 'Martist'
                })

                .when('/songs/:artistid', {
                    templateUrl : 'templates/tmpsongs.html',
                    controller: 'AllSongsCtrl'
                })
                // route for the contact page
                .when('/albumsongs/:albumid', {
                    templateUrl : 'templates/tmpalbumsongs.html',
                    controller  : 'AlbumSongs'
                })
                .when('/topsongs', {
                    templateUrl : 'templates/tmptopsongs.html',
                    controller  : 'TopSongsCtrl'
                })
                .when('/topdownload', {
                    templateUrl : 'templates/tmptopdownload.html',
                    controller  : 'TopDownload'
                })
                .when('/playlist', {
                    templateUrl : 'templates/tmpplaylist.html',
                    //templateUrl : 'templates/tmpindex.html',
                    controller  : 'PlaylistCtrl'
                    //templateUrl : 'templates/tmpplaylist.html',
                })
                .when('/radio', {
                    templateUrl : 'templates/tmpradio.html',
                    controller  : 'RadioCtrl'
                })
                .when('/searchmusic', {
                    templateUrl : 'templates/tmpsearchresults.html',
                    controller  : 'SearchCtrl'
                })
                .when('/hitlist', {
                    templateUrl : 'templates/tmptophitplaylist.html',
                    controller  : 'TopSongsCtrl'
                })
                .otherwise({
                redirectTo: '/home'
            });


        });


/* .config(function($stateProvider, $urlRouterProvider) {

     $stateProvider.state('app', {
         url: '/app',
         abstract: true,
         templateUrl: 'templates/sidemenu.html',
         controller: 'MainCtrl'
     })
     $stateProvider.state('app.songs', {
         url: '/songs:artistid',
         views: {
             'menuContent': {
                 templateUrl: 'templates/songs.html',
                 controller : 'MainCtrl'
             }
         }
     })
     $stateProvider.state('app.albumsongs', {
         url: '/albumsongs/:albumid',
         views: {
             'menuContent': {
                 templateUrl: 'templates/albumsongs.html',
                 controller:'AlbumSongs'
             }
         }

     })
     $stateProvider.state('app.startist', {
         url: '/startist',
         views: {
             'menuContent': {
                 templateUrl: 'templates/startist.html'
             }
         }

     })
     $stateProvider.state('app.salbums', {
         url: '/salbums',
         views: {
             'menuContent': {
                 templateUrl: 'templates/albums.html',
                 controller: 'Malbum'
             }
         }

     })
     $urlRouterProvider.otherwise('/app/songs')
 })*/

})();