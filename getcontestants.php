<?php
require_once('mfunc.php');
$mysqli = new mysqli(HOSTNAME,USERNAME,PASSWORD,DATABASE) or die('Could not connect to db Server' . mysql_error());
$bool = false;
if ($mysqli->connect_error) {
    die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}else{$bool = true;}

if($bool){
    getContestants();
}
function getContestants()
{
    $qd =<<<EOF
SELECT * FROM gmb
EOF;
    $items =[];
    global $mysqli;
    $Result = $mysqli->query($qd) or die($mysqli->error);
    while($row = $Result->fetch_object() ){
        $items[] = array(
            'id' => $row->id,
            'fullname' => $row->fullname,
            'shortname' => $row->shortname,
            'region' => $row->region,
            'smallimg' => $row->smallimg,
            'bigimg' => $row->bigimg,
            'bio' => $row->bio,
            'shortcode' => $row->shortcode
        );
    }
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');
    echo json_encode($items);
}





?>