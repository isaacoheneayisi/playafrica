/**
 * Created by Kofi Essel on 1/29/2016.
 */
(function() {
    var app = angular.module('starter.controllers', []);
    var url ='';
    var redSlider;
    var blueSlider;
    var curl='';
    var hascrd = false;
    var hasverified = false;
    var currentSoundM;
    var currentSongDuration=0;


    app.controller('Malbum',['$http','$scope', function ($http,$scope){
        $scope.albums = [];

        $http.post(url+'stribe/getBums.php').success(function(data){
            $scope.albums = data;
            console.log(data);
        });

    }]);

    app.controller('mSession', ['$cookieStore', function($cookieStore) {
        // Put cookie
        $scope.putSession = function(phone){
            $cookieStore.put('mid',phone);
        };

        // Get cookie
        var favoriteCookie = $cookieStore.get('myFavorite');
        // Removing a cookie
        $cookieStore.remove('myFavorite');
    }]);

    app.controller('Martist',['$http','$scope', function ($http,$scope){
        $scope.rst = [];
        $scope.artists = [];
        $scope.af = '';

        $http.post(url+'stribe/getartist.php').success(function(data){
            $scope.rst = data;
            $scope.artists = $scope.rst;
            console.log(data);
        });

        function strStartsWith(str, prefix) {
            str = str.toLowerCase();
            prefix = prefix.toLowerCase();
            console.log(str.indexOf(prefix) === 0);
            return str.indexOf(prefix) === 0;

        }

        $scope.filterArtist = function(ke){
            $scope.artists = [];
            angular.forEach($scope.rst, function(value, key) {

                if(strStartsWith(value.name, ke)){
                    $scope.artists.push(value);
                    console.log(value);
                }
            });
        }

    }]);

    app.controller('SubmitCardInfo',['$http','$scope',function($http,$scope){

        $scope.p ={};
        $scope.processCard = function() {
            $scope.showPleaseWait();
        console.log($scope.p);
            // Posting data to php file
            console.log("here");
            $http.post(url+'stribe/visamasterpayment.php?customerFirstName=' + $scope.p.customerFirstName + '&amount=' + $scope.p.amount + '&customerLastName=' + $scope.p.customerLastName + '&customerEmail=' + $scope.p.customerEmail + '&customerPhoneNumber=' + $scope.p.customerPhoneNumber + '&pkg=' + localStorage.getItem('pkg'))
                .success(function (data) {
                    $scope.hidePleaseWait();
                    $scope.hideVisaPayment();
                    window.open(data, '_blank');
                    console.log(data);
                    if (data.errors) {

                    } else {

                    }
                });
        };
    }]);

    app.controller('RegisterUser',['$scope','$http',function($scope,$http) {


        $scope.reg = {};
        $scope.registeru = function () {
            $scope.showPleaseWait();
            console.log($scope.reg);
            // Posting data to php file

            $http.post(url+'stribe/register.php?name=' + $scope.reg.name + '&phone=' + $scope.reg.phone + '&password=' + $scope.reg.password)
                .success(function (data) {

                    $scope.hidePleaseWait();
                    console.log(data);
                    if (data.length > 1) {
                        window.localStorage.setItem('mid', data);
                        localStorage.setItem('username',$scope.reg.name);
                        //$cookieStore.put('mid',$scope.reg.phone)
                        $scope.hideRegisterDialog();
                        $scope.showConfirmNumberDialog();
                        //showAlert("User Registration","Registration Successful")

                    } else {
                        //showAlert("User Registration","Registration UnSuccessful")
                    }


                });

        };

    }]);


    app.controller('LoginUser',['$http','$scope',function($http,$scope){
        $scope.lin ={};


        $scope.login = function() {
            $scope.showPleaseWait();
            var pn = $scope.lin.phone;
            console.log($scope.lin);
            // Posting data to php file

            $http.post(url+'stribe/login.php?phone='+pn +'&upass='+ $scope.lin.upass)
                .success(function(data){

                    $scope.hidePleaseWait();
                    console.log(data);
                    if (data == '1') {
                        window.localStorage.setItem('mid',pn);
                        window.localStorage.setItem('confirmed',true);
                        $scope.showLErr = false;
                        $scope.hideLoginDialog();

                    } else {
                        $scope.showLErr = true;
                        $scope.alert = {msg: ' Invalid Username and Password'};
                        //showAlert("Login Status","Login Failed")
                    }

                });

            /*$http({
                method  : 'POST',
                url     : 'http://localhost/soundtrybegh/login.php',
                data    : {phone:$scope.lin.phone,upass:$scope.lin.upass} //forms user object

            })
                .success(function(data) {
                    $ionicLoading.hide();
                    console.log(data);
                    if (data == '1') {
                        window.localStorage.setItem('mid',pn);
                        $scope.modallogin.hide();

                    } else {
                        showAlert("Login Status","Login Failed")
                    }
                });*/
        };

        var showAlert = function(title,msg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });

            alertPopup.then(function(res) {
                console.log('then');
            });
        };
    }]);

    app.controller('ConfirmNumber',['$http','$scope',function($http,$scope){
        $scope.lin ={};

        $scope.confirmCode = function() {
            $scope.showPleaseWait();
            var pn = window.localStorage.getItem('mid');
            console.log($scope.c);
            // Posting data to php file

            $http.post(url+'stribe/confirmcode.php?phone='+pn +'&code='+ $scope.c.code)
                .success(function(data){

                    $scope.hidePleaseWait();
                    console.log(data);
                    if (data == '1') {
                        window.localStorage.setItem('confirmed',true);
                        hasverified=true;
                        $scope.showCErr = false;
                        $scope.hideConfirmNumberDialog();

                    } else {
                        $scope.showCErr = true;
                        hasverified = false;
                        $scope.alertc = {msg: ' Invalid Confirmation Code'};
                        //showAlert("Login Status","Login Failed")
                    }

                });
        };

        var showAlert = function(title,msg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });

            alertPopup.then(function(res) {
                console.log('then');
            });
        };
    }]);

    app.controller('MobileMoneyCtrl',function($scope,$http){
        $scope.data = {};
        $scope.processAirtel = function() {
            $scope.showPleaseWait();

            // An elaborate, custom popup
            $http.post(url+'stribe/airtelmm.php?phone='+$scope.data.phone+'&amount='+$scope.data.amount + '&pkg=' + localStorage.getItem('pkg')+'&accountmobile='+localStorage.getItem('mid'),{timeout:120000}).success(function(data){
                $scope.hidePleaseWait();
                if(data=='1')
                {
                    $scope.hideAirtelPayment();
                    alert("Transaction Successful");

                }else{

                    alert("Transaction Failed");

                }
                console.log(data);
            });


        };

        $scope.processMtn = function() {
            $scope.showPleaseWait();

            // An elaborate, custom popup
            $http.post(url+'stribe/mtnmm.php?mobile='+$scope.data.phone+'&amount='+$scope.data.amount+'&fullname='+localStorage.getItem('username')+'&accountmobile='+localStorage.getItem('mid')+'&isuser=1' + '&pkg=' + localStorage.getItem('pkg'),{timeout:120000}).success(function(data){
                $scope.hidePleaseWait();
                if(data=='1')
                {
                    $scope.hideMtnPayment();
                    alert("Please Complete the Transaction From Your Mobile Money Enabled Device");

                }else{

                    alert("Transaction Failed Please Try Again Later");

                }
                console.log(data);
            });


        };
    });

    app.controller('PopupCtrl',function($scope, $ionicPopup, $timeout) {

// Triggered on a button click, or some other target
        $scope.showPopup = function() {
            $scope.data = {};

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                template: '<input type="password" ng-model="data.wifi">',
                title: 'Enter Wi-Fi Password',
                subTitle: 'Please use normal things',
                scope: $scope,
                buttons: [
                    { text: 'Cancel' },
                    {
                        text: '<b>Save</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            if (!$scope.data.wifi) {
                                //don't allow the user to close unless he enters wifi password
                                e.preventDefault();
                            } else {
                                return $scope.data.wifi;
                            }
                        }
                    }
                ]
            });

            myPopup.then(function(res) {
                console.log('Tapped!', res);
            });

            $timeout(function() {
                myPopup.close(); //close the popup after 3 seconds for some reason
            }, 3000);
        };

        // A confirm dialog
        $scope.showConfirm = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Consume Ice Cream',
                template: 'Are you sure you want to eat this ice cream?'
            });

            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                } else {
                    console.log('You are not sure');
                }
            });
        };

        // An alert dialog
        $scope.showAlert = function(title,msg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });

            alertPopup.then(function(res) {
                console.log('Thank you for not eating my delicious ice cream cone');
            });
        };
    });

    app.controller('AlbumSongs',['$http','$scope','$routeParams', function ($http,$scope,$routeParams) {
        var sposition = 0;

        $scope.albumsongs= [];
        $http.post(url+'stribe/getsongs.php?albumid='+ $routeParams.albumid).success(function(data){
            $scope.albumsongs = data;
            console.log(data);
        });

    }]);

    app.controller('TopSongsCtrl',['$http','$scope', function ($http,$scope) {
        var playindx =0;
//var currentSongDuration =0;
        var cSound;
        $(document).ready(function(){
            loadTopSongs();
        });
        $scope.topsongs= [];
        $http.post(url+'stribe/gettopplayed.php').success(function(data){
            localStorage.setItem('svtopsongs',JSON.stringify(data));
            loadTopSongs();
            //$scope.topsongs = data;
            console.log(data);
        });

        function loadTopSongs(){
            var a = [];
            a=JSON.parse(localStorage.getItem('svtopsongs'));
            //console.log(a);
            if(a !=null) {
                $scope.topsongs = a;
            }
        }

        $scope.playTopListStart = function(s){
            playindx = s;
            $scope.playTopPlaylistSound($scope.topsongs[s])
        };

        $scope.playTopPlaylistSound = function(s) {
            $('#playlisttitle').html(s.title + ' (' + s.name + ')');
            $('.currentPlayingImage').prop('src', s.img);
            $('.topimg').prop('src', s.img);
            $(".jp-play-bar").css('width', '0');
            $('.plplay').removeClass('text-active');
            $('.plpause').addClass('text-active');
            $('#playlistpause' + playindx).removeClass('text-active');
            $('#playlistplay'+ playindx).addClass('text-active');
            $('.musicbar').removeClass('animate');

            var minutes =0;
            var seconds = 0;
            var minutes2 =0;
            var seconds2 = 0;
            soundManager.stopAll();
            soundManager.setup({
                url: 'js/jPlayer',
                onready: function () {
                    var mySound = soundManager.createSound({
                        id: s.id,
                        url: s.url,
                        whileplaying: function () {
                           // $(".jp-seek-bar").css('width', '100%');
                            //$(".jp-play-bar").css('width', ((this.position / this.duration) * 100) + '%');
                            blueSlider.setValue(((this.position/this.duration) * 100));
                            currentSongDuration = this.duration;
                            var current  = new Date (this.position),
                                minutes  = ('0' + current.getMinutes()).slice(-2),
                                seconds  = ('0' + current.getSeconds()).slice(-2);
                            var current2  = new Date (this.duration),
                                minutes2  = ('0' + current2.getMinutes()).slice(-2),
                                seconds2  = ('0' + current2.getSeconds()).slice(-2);

                            var newpos = minutes + ':' + seconds;
                            var newdur = minutes2 + ':' + seconds2;
                            $('.jp-current-time').html(newpos);
                            $('.jp-duration').html(newdur);
                            $(".jp-pause").css('display', 'inline');
                            $(".jp-play").css('display', 'none');
                            $('.musicbar').addClass('animate');
                            //console.log(((this.position/this.duration) * 100) + '%')
                        },
                        onfinish: function () {
                            $(".jp-play-bar").css('width', '0');
                            $(".jp-pause").css('display', 'none');
                            $(".jp-play").css('display', 'inline');
                            $('.musicbar').removeClass('animate');

                            playindx++;
                            if(playindx >= $scope.playlistsongs.length)
                            {
                                playindx=0;
                            }
                            $scope.playPlaylistSound($scope.playlistsongs[playindx]);
                        }
                    });
                    mySound.play();
                    curl = s.url;
                    currentSoundM = mySound;
                    $('.jp-title').html(s.title + ' by ' + s.name);
                    $scope.logPlayed(s.id, window.localStorage.getItem('mid'));

                    /*$(".jp-seek-bar").click(function(e){
                     var $this = $(this);
                     var relX = e.pageX - parseFloat($this.offset().left);

                     var time = currentSongDuration*(relX / $this.width());
                     console.log('seek position '+time);
                     cSound.setPosition(time);
                     });*/
                },
                ontimeout: function () {
                    // Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
                }
            });
        };
    }]);

    app.controller('TopDownload',['$http','$scope', function ($http,$scope) {

        $scope.topdownload= [];
        $(document).ready(function(){
            loadTopDownloads();
        });

        $http.post(url+'stribe/gettopdownload.php').success(function(data){
            //$scope.topdownload = data;
            localStorage.setItem('svtopdownloads',JSON.stringify(data));
            loadTopDownloads();
            //console.log(data);
        });

        function loadTopDownloads(){
            var a = [];
            a=JSON.parse(localStorage.getItem('svtopdownloads'));
            //console.log(a);
            if(a !=null) {
                $scope.topdownload = a;
            }
        }

    }]);

    app.controller('NewSongsCtrl',['$http','$scope', function ($http,$scope) {

        $scope.newsongs= [];
        $(document).ready(function(){
            loadNewSongs();
        });

        $http.post(url+'stribe/getnewsongs.php').success(function(data){
            localStorage.setItem('svnewsongs',JSON.stringify(data));
            loadNewSongs();
            //$scope.newsongs = data;
            //console.log(data);
        });

        function loadNewSongs(){
            var a = [];
            a=JSON.parse(localStorage.getItem('svnewsongs'));
            //console.log(a);
            if(a !=null) {
                $scope.newsongs = a;
            }
        }
    }]);

    app.controller('AllSongsCtrl',['$http','$scope','$routeParams', function ($http,$scope,$routeParams) {
$scope.loadbusy = false;
        $scope.allsongs= [];
        $http.post(url+'stribe/getsongs.php?artistid='+ $routeParams.artistid).success(function(data){
            $scope.allsongs = data;
            console.log(data);
        });

        var i=11;
        $scope.loadmore = function(){
            if($scope.allsongs.length<9) return;
            if ($scope.loadbusy) return;
            $scope.loadbusy = true;

            $http.post(url+'stribe/getsongs.php?artistid='+ $routeParams.artistid + '&offset='+ i).success(function(items) {
                if (items == null) {

                    for(var j=0; j<items.length; j++)
                    {

                        $scope.allsongs.push(items[j]);

                    }
                }
                console.log(i);
                console.log(items);
                $scope.loadbusy=false;
            });
            i+=10;
        };
    }]);

    app.controller('SearchCtrl',['$http','$scope','$routeParams', function ($http,$scope,$routeParams) {
        var searchkey = $('.mkey').val();
        $scope.searchresults =[];


            console.log(searchkey);
            $http.post(url + 'stribe/getsearchmusic.php?searchkey=' + searchkey).success(function (data) {
                $scope.searchresults = data;
                console.log(data);
            });

        $('.musicsearch').submit(function(){
            var searchkey = $('.mkey').val();
            $http.post(url + 'stribe/getsearchmusic.php?searchkey=' + searchkey).success(function (data) {
                $scope.searchresults = data;
                console.log(data);
            });
        });


    }]);

    app.controller('GenreCtrl',['$http','$scope', function ($http,$scope) {
        $scope.genre= [];
        $http.post(url+'stribe/getgenre.php').success(function(data){
            $scope.genre = data;
            console.log(data);
        });
    }]);

    app.controller('PlaylistCtrl',['$scope',function($scope){
var playindx =0;
//var currentSongDuration =0;
        var cSound;
       $scope.playlistsongs = [];
        if(window.localStorage.getItem('playlist')){
            $scope.playlistsongs = JSON.parse(window.localStorage.getItem('playlist'));
            console.log($scope.playlistsongs);
        }

        $scope.removeFromPlaylist = function(s){
            $scope.playlistsongs.splice(s,1);
            window.localStorage.setItem('playlist',JSON.stringify($scope.playlistsongs));
        };

        $scope.playListStart = function(s){
          playindx = s;

            if($scope.testTimes()>10){
                if( window.localStorage.getItem('mid'))
                {
                    if(window.localStorage.getItem('confirmed')){

                        $scope.promise = $scope.hasCredit();
                        $scope.promise.then(
                            function(v){
                                if(v=='1')
                                {
                                    console.log(true);
                                    $scope.playPlaylistSound($scope.playlistsongs[s])
                                }else{
                                    $scope.showSubscriptionType();
                                }
                            },
                            function(err){
                                console.log(err);
                                return false;
                            }
                        );

                        /*console.log($scope.getC());
                         if($scope.getC()){

                         }else{
                         $scope.showSubscriptionType();
                         *//*$scope.showPayOptions();*//*
                         return
                         }*/

                    }else{
                        //hasverified =false
                        $scope.showConfirmNumberDialog();
                        return;
                    }

                }else{
                    $scope.showRegisterDialog();
                    // hascrd = false;
                    return;
                }
            }else{

            }

        };

        $scope.playPlaylistSound = function(s) {
            $('#playlisttitle').html(s.title + ' (' + s.name + ')');
            $('.currentPlayingImage').prop('src', s.img);
            $('.topimg').prop('src', s.img);
            $(".jp-play-bar").css('width', '0');
            $('.plplay').removeClass('text-active');
            $('.plpause').addClass('text-active');
            $('#playlistpause' + playindx).removeClass('text-active');
            $('#playlistplay'+ playindx).addClass('text-active');
            $('.musicbar').removeClass('animate');

            var minutes =0;
            var seconds = 0;
            var minutes2 =0;
            var seconds2 = 0;
            soundManager.stopAll();
            soundManager.setup({
                url: 'js/jPlayer',
                onready: function () {
                    var mySound = soundManager.createSound({
                        id: s.id,
                        url: s.url,
                        whileplaying: function () {
                            //$(".jp-seek-bar").css('width', '100%');
                            //$(".jp-play-bar").css('width', ((this.position / this.duration) * 100) + '%');
blueSlider.setValue(((this.position/this.duration) * 100));
                            currentSongDuration = this.duration;
                            var current  = new Date (this.position),
                                minutes  = ('0' + current.getMinutes()).slice(-2),
                                seconds  = ('0' + current.getSeconds()).slice(-2);
                            var current2  = new Date (this.duration),
                                minutes2  = ('0' + current2.getMinutes()).slice(-2),
                                seconds2  = ('0' + current2.getSeconds()).slice(-2);

                            var newpos = minutes + ':' + seconds;
                            var newdur = minutes2 + ':' + seconds2;
                            $('.jp-current-time').html(newpos);
                            $('.jp-duration').html(newdur);
                            $(".jp-pause").css('display', 'inline');
                            $(".jp-play").css('display', 'none');
                            $('.musicbar').addClass('animate');
                            //console.log(((this.position/this.duration) * 100) + '%')
                        },
                        onfinish: function () {
                            $(".jp-play-bar").css('width', '0');
                            $(".jp-pause").css('display', 'none');
                            $(".jp-play").css('display', 'inline');
                            $('.musicbar').removeClass('animate');

                            playindx++;
                            if(playindx >= $scope.playlistsongs.length)
                            {
                                playindx=0;
                            }
                            $scope.playPlaylistSound($scope.playlistsongs[playindx]);
                        }
                    });
                    mySound.play();
                    curl = s.url;
                    currentSoundM = mySound;
                    $('.jp-title').html(s.title + ' by ' + s.name);
                    $scope.logPlayed(s.id, window.localStorage.getItem('mid'));

                    /*$(".jp-seek-bar").click(function(e){
                        var $this = $(this);
                        var relX = e.pageX - parseFloat($this.offset().left);

                        var time = currentSongDuration*(relX / $this.width());
                        console.log('seek position '+time);
                        cSound.setPosition(time);
                    });*/
                },
                ontimeout: function () {
                    // Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
                }
            });
        };



    }]);

    app.controller('RadioCtrl',['$scope','$interval',function($scope,$interval){
    //playRadio({id:'radio',url:'http://uk3.internet-radio.com:8213/live',title:'Mtech Radio'});
        var currentSongDuration =0;
        var cSound;
        var rad = false;
        $('.radbtn').on('click',function(){
           $(this).hide();
        });

        $('.radbtn').show();
        $(document).ready(function(){
            $scope.playRadio({id:'radio',url:'http://hyades.shoutca.st:8069/live',title:'Mtech Radio',name:''});
        });


    }]);

    app.controller('ChatCtrl',['$http','$scope',function($http,$scope){
        //$scope.nd = new Date(2016, 02, 10);
        $scope.msg='';
        $scope.chats =[];
        $http.post(url+'stribe/getchats.php').success(function(data){
            $scope.chats = data;
            console.log(data);
            /*var $id = $('#bottom');
            $id.scrollTop($id[0].scrollHeight+60);*/
        });
        //$scope.chats.push({name:'Gideon',comment:'i love this station',ctime:'2hr ago',userid:'0246144043'},{name:'Isaac',comment:'DJ u be the guy!',ctime:'1hr ago'},{name:'Isaac',comment:'DJ u be the guy!',ctime:'1hr ago'});

        $scope.toggleLeftOrRight = function(id,k){
            if(k == $scope.user_id){

                return 'right';
            }else{

                return 'left';
            }


        };

        $scope.sendChat = function(){
            var nowchat =  new Date();
            if($scope.msg.length>0)
            {
                $scope.chats.push({name:$scope.username,comment:$scope.msg,ctime:nowchat,userid:$scope.user_id});
                $http.post(url+'stribe/logchat.php?username='+ $scope.username +'&userid='+$scope.user_id + '&comment='+$scope.msg).success(function(data){
                    if(data.length<2){

                    }else{
                        $scope.chats.concat(data);
                    }

                    console.log(data);
                });
                $scope.msg = '';
            }

        }
    }]);

    app.controller('ConnectedCtrl',['$http','$scope',function($http,$scope){
        //$scope.nd = new Date(2016, 02, 10);
        $scope.people=[];

        $http.post(url+'stribe/getuseractivity.php').success(function(data){
            $scope.people = data;
            console.log(data);
        });

    }]);

    app.directive("scrollBottom", ['$timeout', function($timeout){
        return {
            link: function(scope, element, attr){
                var $id= $("#" + attr.scrollBottom);
                $(element).on("click", function(){
                    $timeout(function () {
                        $id.scrollTop($id[0].scrollHeight+60);
                    });
                });
            }
        }
    }]);

    app.filter('unique', function() {
        return function(collection, keyname) {
            var output = [],
                keys = [];

            angular.forEach(collection, function(item) {
                var key = item[keyname];
                if(keys.indexOf(key) === -1) {
                    keys.push(key);
                    output.push(item);
                }
            });
            return output;
        };
    });

    app.controller('MainCtrl',['$http','$routeParams','$scope','$interval',function($http,$routeParams,$scope,$interval) {



        $scope.trackreached = 0;
        $scope.songs= [];
        $(document).ready(function(){
            loadSongs();
        });
        $scope.loadSongs = function() {
            $http.post(url+'stribe/getsongs.php?artistid=' + $routeParams.artistid, {timeout: 120000}).success(function (data) {
                localStorage.setItem('svsongs',JSON.stringify(data));
                loadSongs();
               /* $scope.songs = data;
                console.log(data);*/

            });
        };

        function loadSongs(){
            var a = [];
            a=JSON.parse(localStorage.getItem('svsongs'));
            //console.log(a);
            if(a !=null) {
                $scope.songs = a;
            }
        }




        angular.element(document).ready(function () {

            $scope.loadSongs();

        });



    }]);

    app.controller('mctr',['$http','$scope','$interval','$q', function ($http,$scope,$interval,$q) {

        $scope.user_id='';
        $scope.username='';
        $scope.curl ='';
        $scope.hascrbt = false;
        $(".jp-play-bar").css('width', '0');
        $(function () {
            var pleaseWait = $('#pleaseWaitDialog');
            var loginDialog = $('#loginDialog');
            var registerDialog = $('#registerDialog');
            var payOptDialog = $('#payOptions');
            var payCards = $('#payCards');
            var visaPayment = $('#visaPayment');
            var airtelPayment = $('#airtelPayment');
            var mtnPayment = $('#mtnPayment');
            var confirmNumberDialog = $('#confirmNumberDialog');
            var subscriptionType = $('#subscription_type');

            $scope.showConfirmNumberDialog = function() {
                confirmNumberDialog.modal('show');
            };

            $scope.showSubscriptionType = function(){
                subscriptionType.modal('show');
            };

            $scope.hideSubscriptionType = function(){
              subscriptionType.modal('hide');
            };

            $scope.hideConfirmNumberDialog = function () {
                confirmNumberDialog.modal('hide');
            };

            $scope.showAirtelPayment = function() {
                airtelPayment.modal('show');
            };

            $scope.hideAirtelPayment = function () {
                airtelPayment.modal('hide');
            };

            $scope.hideMtnPayment = function () {
                mtnPayment.modal('hide');
            };

            $scope.showMtnPayment = function() {
                mtnPayment.modal('show');
            };

            $scope.showVisaPayment = function() {
                visaPayment.modal('show');
            };

            $scope.hideVisaPayment = function () {
                visaPayment.modal('hide');
            };

            $scope.showPayCards = function() {
                payCards.modal('show');
            };

            $scope.hidePayCards = function () {
                payCards.modal('hide');
            };

            $scope.showPayOptions = function() {
                payOptDialog.modal('show');
            };

            $scope.hidePayOptions = function () {
                payOptDialog.modal('hide');
            };

            $scope.showPleaseWait = function() {
                pleaseWait.modal('show');
            };

            $scope.hidePleaseWait = function () {
                pleaseWait.modal('hide');
            };

            $scope.showLoginDialog = function() {
                loginDialog.modal('show');
            };

            $scope.hideLoginDialog = function () {
                loginDialog.modal('hide');
            };

            $scope.showRegisterDialog = function() {
                registerDialog.modal('show');
            };

            $scope.hideRegisterDialog = function () {
                registerDialog.modal('hide');
            };


            $scope.closeAlert = function() {
                $scope.showLErr = false;
            };

            $(document).ready(function () {
                $interval($scope.downloadmp3,1500);

                //$interval($('#seekbar-container-vertical-blue').css('width','95%'),1500);

            });


            var canDownload = false;
            $scope.initiateDownload = function(fileurl){
                curl = fileurl;
                $scope.showPleaseWait();
                $http.post(url+'stribe/checks.php?accbal=0&phone='+ window.localStorage.getItem('mid') + '&act=download&pkg='+ localStorage.getItem('pkg')).success(function(data){
                    // var p = document.getElementById('btnpl');
                    $scope.hidePleaseWait();

                    if (data=='1'){

                        canDownload=true;
//
                    }else{
                        $scope.showPayOptions();
                    }
                    console.log(data);

                });

            };

            $scope.downloadmp3 = function(){
              if(canDownload){
                  window.open(curl,'_self');
                  canDownload=false;
              }
            };

            $scope.initiateDownloadBottom = function(){

                if(curl.length < 1){
                    return;
                }
                $scope.showPleaseWait();
                $http.post(url+'stribe/checks.php?accbal=0&phone='+ window.localStorage.getItem('mid') + '&act=download&pkg='+ localStorage.getItem('pkg')).success(function(data){

                    $scope.hidePleaseWait();

                    if (data=='1'){

                        canDownload=true;

                    }else{
                        $scope.showPayOptions();

                    }
                    console.log(data);
                });
            };

            function saveAs(url) {
                var filename = url.substring(url.lastIndexOf("/") + 1).split("?")[0];
                var xhr = new XMLHttpRequest();
                xhr.responseType = 'blob';
                xhr.onload = function() {
                    var a = document.createElement('a');
                    a.href = window.URL.createObjectURL(xhr.response);
                    a.download = filename;
                    a.target = '_self';
                    a.style.display = 'none';
                    document.body.appendChild(a);
                    a.click();
                    delete a;
                };
                xhr.open('GET', url);
                xhr.send();
            }


        });

        $scope.setPackage = function(pkg){
            window.localStorage.setItem('pkg',pkg);
            $scope.hideSubscriptionType();
            if(pkg == "payg"){
                $scope.showPayOptions();
            }else{
                $scope.showPayCards();
            }
        };

        $scope.getC = function(){
          $scope.promise = $scope.hasCredit();
            $scope.promise.then(
                function(v){
                    if(v=='1')
                    {
                        return true;
                    }else{
                        return false;
                    }
                },
                function(err){
                    console.log(err);
                    return false;
                }
            )
        };

        $scope.hasCredit = function(){

            var defer = $q.defer();
            var http = $http({
                url: url+'stribe/checks.php',
                method: 'POST',
                params: {"accbal":0,"phone":window.localStorage.getItem('mid'),"act":"play","pkg":localStorage.getItem('pkg') }
            });

            http.success(function(stats) {
                defer.resolve(stats);
            })
                .error(function() {
                    defer.reject("Failed to get stats");
                });

            return defer.promise;

            /*$http.post(url+'stribe/checks.php?accbal=0&phone='+ window.localStorage.getItem('mid') + '&act=play'+'&pkg='+ localStorage.getItem('pkg'), {timeout: 120000}).success(function (data) {
                //console.log(data);
                if (data=='1'){
                    console.log(true);
                    return true;
                }else{
                    return false;
                }
            });*/
        };

        $scope.logPlayed = function(songid,userid){
            $http.post(url+'stribe/logplayed.php?songid='+songid + '&userid='+userid, {timeout: 120000}).success(function (data) {
                console.log(data);


            });
        };

        $scope.logdownload = function(songid,userid){
            $http.post(url+'stribe/logdownloaded.php?songid='+songid + '&userid='+userid, {timeout: 120000}).success(function (data) {
                console.log(data);


            });
        };

        $scope.isRegistered = function(){
          if(window.localStorage.getItem('mid')){
              return true;
          }else{
              return false;
          }
        };


        $scope.playAudioNow = function(audioInfo){
            $('.currentPlayingImage').prop('src',audioInfo.img);
            $('#playlisttitle').html(audioInfo.title + ' (' + audioInfo.name + ')');
            $('.topimg').prop('src',audioInfo.img);
            $(".jp-play-bar").css('width', '0');
            $(".jp-pause").css('display','none');
            $(".jp-loading").css('display','inline');
            $(".jp-play").css('display','none');

            $('.plplay').removeClass('text-active');
            $('.plpause').addClass('text-active');
            $('#nxtpause' + audioInfo.id).removeClass('text-active');
            $('#nxtplay'+ audioInfo.id).addClass('text-active');
            $('.musicbar').removeClass('animate');

            var minutes =0;
            var seconds = 0;
            var minutes2 =0;
            var seconds2 = 0;
            soundManager.stopAll();

            soundManager.setup({
                url: 'js/jPlayer',
                onready: function() {
                    var mSound = soundManager.createSound({
                        id: audioInfo.id,
                        url: audioInfo.url,
                        whileplaying: function() {
                            currentSongDuration = this.duration;
                            //$(".jp-seek-bar").css('width','100%');
                            //$(".jp-play-bar").css('width', ((this.position/this.duration) * 100) + '%');
                            blueSlider.setValue(((this.position/this.duration) * 100));
                            $(".radbtn").hide();


                            var current  = new Date (this.position),
                                minutes  = ('0' + current.getMinutes()).slice(-2),
                                seconds  = ('0' + current.getSeconds()).slice(-2);
                            var current2  = new Date (this.duration),
                                minutes2  = ('0' + current2.getMinutes()).slice(-2),
                                seconds2  = ('0' + current2.getSeconds()).slice(-2);

                            var newpos = minutes + ':' + seconds;
                            var newdur = minutes2 + ':' + seconds2;
                            console.log(this.duration);
                            //console.log(((this.position/this.duration) * 100) + '%')
                            $('.jp-current-time').html(newpos);
                            $('.jp-duration').html(newdur);
                            $(".jp-pause").css('display','inline');
                            $(".jp-loading").css('display','none');
                            $(".jp-play").css('display','none');
                            $('.musicbar').addClass('animate');

                        },
                        onfinish: function() {
                            $(".jp-play-bar").css('width', '0');
                            $(".jp-pause").css('display','none');
                            $(".jp-loading").css('display','none');
                            $(".jp-play").css('display','inline');
                            $('.musicbar').removeClass('animate');
                            $(".radbtn").show();
                        }
                    });
                    mSound.play();
                    curl = audioInfo.url;
                    currentSoundM = mSound;
                    $('.title').html('Title: '+ audioInfo.title);
                    $('.artist').html('Artist: '+ audioInfo.name);
                    $scope.logPlayed(audioInfo.id,window.localStorage.getItem('mid'));

                    /*$(".seekbar-container-vertical-blue").click(function(e){
                     var $this = $(this);
                     var relX = e.pageX - parseFloat($this.offset().left);

                     var time = currentSongDuration*(relX / $this.width());
                     console.log('seek '+ time);

                     });*/
                },
                ontimeout: function() {
                    // Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
                }
            });
        };

        $scope.playAudio = function(audioInfo){




            if($scope.testTimes()>10){
                if( window.localStorage.getItem('mid'))
                {
                    if(window.localStorage.getItem('confirmed')){

                        $scope.promise = $scope.hasCredit();
                        $scope.promise.then(
                            function(v){
                                if(v=='1')
                                {
                                    $scope.playAudioNow(audioInfo);
                                }else{
                                    $scope.showSubscriptionType();
                                }
                            },
                            function(err){
                                console.log(err);
                                return false;
                            }
                        );

                        /*console.log($scope.getC());
                        if($scope.getC()){

                        }else{
                            $scope.showSubscriptionType();
                            *//*$scope.showPayOptions();*//*
                            return
                        }*/

                    }else{
                        //hasverified =false
                        $scope.showConfirmNumberDialog();
                        return;
                    }

                }else{
                    $scope.showRegisterDialog();
                   // hascrd = false;
                    return;
                }
            }else{
                $scope.playAudioNow(audioInfo);
            }

            console.log(audioInfo.hascrbt + ' has crb5');
            if(audioInfo.hascrbt == '1'){
                $scope.hascrbt = true;

            }else
            {
                $scope.hascrbt =false;
            }

            };

        $scope.pauseAudio = function(){
          soundManager.pauseAll();
            $(".jp-pause").css('display','none');
            $(".jp-play").css('display','inline');
            $('.plplay').removeClass('text-active');
            $(".jp-loading").css('display','none');
            $('.plpause').addClass('text-active');
            $('.musicbar').removeClass('animate');
        };

        $scope.stopAudio = function(){
            soundManager.stopAll();
            $('.musicbar').removeClass('animate');
        };

        $scope.resumeAudio = function(){
            soundManager.resumeAll();
        };

        $scope.addToPlaylist = function(s){
            console.log(s);
            var hasdup = false;
            var pls = JSON.parse(window.localStorage.getItem('playlist'));
            if(pls == null){
                pls = [];
                pls.push(s);
            }else{
                for(var j=0; j<pls.length; j++)
                {

                    if(pls[j].id == s.id){
                        hasdup = true;
                        console.log('duplicate');
                    }else{

                    }

                }

                if(hasdup){

                }else{
                    pls.push(s);
                    console.log('pushed');
                }
            }
            window.localStorage.setItem('playlist',JSON.stringify(pls));
        };

        $scope.testTimes = function(){
            var i = 0;
            if(window.localStorage.getItem('tt'))
            {
                i = window.localStorage.getItem('tt');
                i++;
                window.localStorage.setItem('tt',i);
            }else{
                window.localStorage.setItem('tt',0);
            }
            return window.localStorage.getItem('tt');
        };

        $(document).ready(function(){
            $scope.setProfilePic = function() {
                $('.profilepic').initial({
                    name: 'Name', // Name of the user
                    charCount: 2, // Number of characherts to be shown in the picture.
                    textColor: '#ffffff', // Color of the text
                    seed: 0, // randomize background color
                    height: 60,
                    width: 60,
                    fontSize: 30,
                    fontWeight: 400,
                    fontFamily: 'HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica, Arial,Lucida Grande, sans-serif',
                    radius: 0
                });
            };
            $scope.user_id=localStorage.getItem('mid');
            if(localStorage.getItem('username')){
                $scope.username = localStorage.getItem('username');
            }else{
                $scope.username = 'Guest User'
            }

            //$('#seekbar-container-vertical-blue').css('width',$('.dk').css('width'))

        });

        $scope.setchatpic = function(s,n,i){
            $('#chatpic'+i).initial({
                name: n, // Name of the user
                charCount: 2, // Number of characherts to be shown in the picture.
                textColor: '#ffffff', // Color of the text
                seed: s, // randomize background color
                height: 50,
                width: 50,
                fontSize: 26,
                fontWeight: 400,
                fontFamily: 'HelveticaNeue-Light,Helvetica Neue Light,Helvetica Neue,Helvetica, Arial,Lucida Grande, sans-serif',
                radius: 0
            });
        };
        var w = window.innerWidth;
        if(w > 768) {
            $('#seekbar-container-vertical-blue').css('width', '82%');
            console.log(w);
        }
        //$interval($('.dhtmlgoodies-seekbar').css('width',$('#seekbar-container-vertical-blue').css('width')),1500);

        blueSlider = new Seekbar.Seekbar({
            renderTo: "#seekbar-container-vertical-blue",
            minValue: 1, maxValue: 100,
            valueListener: function (value) {
                this.value = Math.round(value);
                //updateColorPreview();
                currentSoundM.setPosition(currentSongDuration * (value/100))
                //console.log(value)
            },
            thumbColor: '#64a4d0',
            negativeColor: '#006699',
            positiveColor: '#CCC',
            value: 0
        });


        $scope.playRadio = function(audioInfo){
            //$('.currentPlayingImage').prop('src',audioInfo.img);
            $('#playlisttitle').html(audioInfo.title + ' (' + audioInfo.name + ')');
            //$('.topimg').prop('src',audioInfo.img);
            $(".jp-play-bar").css('width', '0');
            $(".jp-pause").css('display','none');
            $(".jp-loading").css('display','inline');
            $(".jp-play").css('display','none');

            $('.plplay').removeClass('text-active');
            $('.plpause').addClass('text-active');
            $('#nxtpause' + audioInfo.id).removeClass('text-active');
            $('#nxtplay'+ audioInfo.id).addClass('text-active');
            $('.musicbar').removeClass('animate');

            var minutes =0;
            var seconds = 0;
            var minutes2 =0;
            var seconds2 = 0;
            soundManager.stopAll();
            soundManager.setup({
                url: 'js/jPlayer',
                onready: function() {
                    var mSound = soundManager.createSound({
                        id: audioInfo.id,
                        url: audioInfo.url,
                        whileplaying: function() {
                            currentSongDuration = 60000000;
                            //$(".jp-seek-bar").css('width','100%');
                            //$(".jp-play-bar").css('width', ((this.position/this.duration) * 100) + '%');
                            blueSlider.setValue(((this.position/currentSongDuration) * 100));
                            $(".radbtn").hide();


                            var current  = new Date (this.position),
                                minutes  = ('0' + current.getMinutes()).slice(-2),
                                seconds  = ('0' + current.getSeconds()).slice(-2);
                            var current2  = new Date (60000000),
                                minutes2  = ('0' + current2.getMinutes()).slice(-2),
                                seconds2  = ('0' + current2.getSeconds()).slice(-2);

                            var newpos = minutes + ':' + seconds;
                            var newdur = minutes2 + ':' + seconds2;
                            console.log(this.duration);
                            //console.log(((this.position/this.duration) * 100) + '%')
                            $('.jp-current-time').html(newpos);
                            $('.jp-duration').html(newdur);
                            $(".jp-pause").css('display','inline');
                            $(".jp-loading").css('display','none');
                            $(".jp-play").css('display','none');
                            $('.musicbar').addClass('animate');

                        },
                        onfinish: function() {
                            $(".jp-play-bar").css('width', '0');
                            $(".jp-pause").css('display','none');
                            $(".jp-loading").css('display','none');
                            $(".jp-play").css('display','inline');
                            $('.musicbar').removeClass('animate');
                            $(".radbtn").show();
                        }
                    });
                    mSound.play();
                    curl = audioInfo.url;
                    currentSoundM = mSound;
                    $('.title').html('Mtech Radio');
                    $('.artist').html('');
                    $scope.logPlayed(audioInfo.id,window.localStorage.getItem('mid'));
                },
                ontimeout: function() {
                    // Hrmm, SM2 could not start. Missing SWF? Flash blocked? Show an error, etc.?
                }
            });

        };

        $(document).ready(function(){

            var w = window.innerWidth;
            if(w > 768) {
                $('.topnav').addClass('navbar-fixed-top-xs');
                $('.topnav').removeClass('navbar-fixed-top');
                console.log(w);
            }else{
                $('.topnav').addClass('navbar-fixed-top');
                $('.topnav').removeClass('navbar-fixed-top-xs');
            }
        });

        $(window).resize(function(){
            var w = window.innerWidth;
            if(w > 768) {
                $('.topnav').addClass('navbar-fixed-top-xs');
                $('.topnav').removeClass('navbar-fixed-top');
                console.log(w);
            }else{
                $('.topnav').addClass('navbar-fixed-top');
                $('.topnav').removeClass('navbar-fixed-top-xs');
            }
        });


    }]);

    app.directive('modal', function () {
        return {
            template: '<div class="modal fade">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                '<h4 class="modal-title">{{ title }}</h4>' +
                '</div>' +
                '<div class="modal-body" ng-transclude></div>' +
                '</div>' +
                '</div>' +
                '</div>',
            restrict: 'E',
            transclude: true,
            replace:true,
            scope:true,
            link: function postLink(scope, element, attrs) {
                scope.title = attrs.title;

                scope.$watch(attrs.visible, function(value){
                    if(value == true)
                        $(element).modal('show');
                    else
                        $(element).modal('hide');
                });

                $(element).on('shown.bs.modal', function(){
                    scope.$apply(function(){
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function(){
                    scope.$apply(function(){
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });


})();

